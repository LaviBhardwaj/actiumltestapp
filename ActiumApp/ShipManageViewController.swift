//
//  ShipManageViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 25/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit

class ShipManageViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Views LIfe cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

         let nib = UINib(nibName: "UpdateTableViewCell", bundle: nil)
         tableView.register(nib, forCellReuseIdentifier: "TableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

    // MARK: - tableView Delegate and Datasource

extension ShipManageViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell")
        return cell!
    }
    
    
}
