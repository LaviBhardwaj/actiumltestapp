//
//  ZoneCollectionViewCell.swift
//  ActiumApp
//
//  Created by Rahul Mishra on 04/02/19.
//  Copyright © 2019 Techsaga Corporations. All rights reserved.
//

import UIKit

class ZoneCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var zoneNameLbl: UILabel!
    @IBOutlet weak var zoneImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.zoneImage.layer.cornerRadius = zoneImage.frame.size.height/2
        self.zoneImage.layer.masksToBounds = true
        roundView.layer.borderWidth = 2
        roundView.layer.borderColor = UIColor.white.cgColor
        roundView.layer.cornerRadius = 15
    }

}
