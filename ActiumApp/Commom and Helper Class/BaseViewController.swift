//
//  BaseViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 30/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

       self.navigationController?.navigationBar.tintColor = UIColor.black
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
