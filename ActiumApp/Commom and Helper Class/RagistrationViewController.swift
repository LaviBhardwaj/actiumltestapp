//
//  RagistrationViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 22/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit

class RagistrationViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - IBOutlet
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet var roundView: [UIView]!
    @IBOutlet weak var cnfrmPassTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    
    // MARK: - View controler life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        signUpBtn.layer.cornerRadius = signUpBtn.frame.size.height/2

        for item in roundView {
            item.layer.borderColor = UIColor.white.cgColor
            item.layer.borderWidth = 1
            item.layer.cornerRadius = 15
        }
        
        // MARK: - Placeholder for text fields
        nameTF.attributedPlaceholder =
            NSAttributedString(string: "Name",                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        emailTF.attributedPlaceholder =
            NSAttributedString(string: "Email Id",                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        phoneTF.attributedPlaceholder =
            NSAttributedString(string: "Phone",                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        passwordTF.attributedPlaceholder =
            NSAttributedString(string: "Password",                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        cnfrmPassTF.attributedPlaceholder =
            NSAttributedString(string: "Confirm Password",                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK: - Textfield Delegate
  func textFieldDidEndEditing(_ textField: UITextField) {
        
     }
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return self.view.endEditing(true)
     }
    
    // MARK: -IBAction

    @IBAction func backSignInActn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
}
