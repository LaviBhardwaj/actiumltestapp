//
//  DataSyncManager.swift
//  ActiumApp
//
//  Created by Rahul Mishra on 14/02/19.
//  Copyright © 2019 Techsaga Corporations. All rights reserved.
//

import UIKit
import Alamofire
import UserNotifications

class DataSyncManager: NSObject, UNUserNotificationCenterDelegate {
    
    static var content = UNMutableNotificationContent()
    static var uploadingImage = [UploadingImages]()
    static var uploadingVideo = [UplodingVideo]()
    static var uploadingReport = [UploadingReport]()
    static var uploadingInventory = [UploadingInventory]()
    
    class func notification(message:String) {
        self.content.title = "Message"
        self.content.body = message
        self.content.badge = 1
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        //getting the notification request
        let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: self.content, trigger: trigger)
        
        //UNUserNotificationCenter.current().delegate = UIApplication.shared.windows.last as? UNUserNotificationCenterDelegate
        
        //adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
 
    
    class func uploadSavedImages() {
        self.uploadingImage = DataBaseHelper.shareInstance.getUploadingImages()
        if self.uploadingImage.count == 0 {
            return
        } else {
            let visitId = uploadingImage[0].visitId
            let visitDate = uploadingImage[0].visitDate
            let zoneName = uploadingImage[0].zoneName
            let shipName = uploadingImage[0].shipName
            let retailerName = uploadingImage[0].retailername
            let imageString = uploadingImage[0].uploadingImage
        
            let params = ["visit_id":visitId!,
                      "zone_name":zoneName!,
                      "retailer_name":retailerName!,
                      "ship_name":shipName!,
                      "visit_date":visitDate!,
                      "img_name": imageString!
            ] as [String : Any]
        
        CommonClass.getDataFromServer("uploadMultipleImages", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") { result -> Void in
            if let dictResponse = result as? [String: Any] {
                if let status = dictResponse["status"] as? Bool,
                    status == true {
                    DataBaseHelper.shareInstance.deleteUploadingImages()
                    let message = dictResponse["message"] as? String
                    notification(message: message!)
                    
                }
            }
        }
    }
    }
    
    class func uploadSavedVideo() {
        
        self.uploadingVideo = DataBaseHelper.shareInstance.getUploadingVideo()
        if self.uploadingVideo.count == 0 {
            return
        }
        else {
            let visitId = uploadingVideo[0].visitId
            let visitDate = uploadingVideo[0].visitdate
            let zoneName = uploadingVideo[0].zoneName
            let shipName = uploadingVideo[0].shipName
            let retailerName = uploadingVideo[0].retailerName
            let urlVideo = uploadingVideo[0].videoUrl
        
        let fullUrl :String = CommonClass.baseURL + "uploadVideo"
        let params = ["visit_id":visitId,
                      "visit_date":visitDate,
                      "zone_name":zoneName,
                      "retailer_name":retailerName,
                      "ship_name":shipName] as [String : AnyObject]
            
            self.multipartDataService(url: fullUrl, data: urlVideo as Any, withName: "video", parameters: params, success: { (response) in
            if let response1 = response as? [String: Any] {
                if let status = response1["status"] as? Bool,
                    status == true {
                    DataBaseHelper.shareInstance.deleteUploadingVideo()
                    let message = response1["message"] as? String
                    self.notification(message: message!)
                }
            }
        }) { (error) in
            print(error)
        }
    }
}
    
    class func uploadSavedReport() {
        
         self.uploadingReport = DataBaseHelper.shareInstance.getUploadingReport()
        if self.uploadingReport.count == 0 {
            return
        } else {
            let visitID = uploadingReport[0].visitId
            let visitDAte = uploadingReport[0].visitDate
            let zoneName = uploadingReport[0].zoneName
            let shipName = uploadingReport[0].shipName
            let retailerName = uploadingReport[0].retailerName
            let myUrl = uploadingReport[0].uploadingReport
        
        let fullUrl :String = CommonClass.baseURL + "uploadReport"
        
        let params = ["visit_id":visitID,
                      "visit_date":visitDAte,
                      "zone_name":zoneName,
                      "retailer_name":retailerName,
                      "ship_name":shipName] as [String : AnyObject]
        print(params)
        
            self.multipartDataService(url: fullUrl, data: myUrl!, withName: "file_name", parameters: params, success: { (response) in
                
            if let response1 = response as? [String: Any] {
                if let status = response1["status"] as? Bool,
                    status == true {
                    DataBaseHelper.shareInstance.deleteUploadingReport()
                    let message = response1["message"] as? String
                    notification(message: message!)
                }
            }
        }) { (error) in
            print(error)
        }
    }
    }
    
    class func uploadSavedInventory() {
        
        self.uploadingInventory = DataBaseHelper.shareInstance.getUploadingInventory()
        if self.uploadingInventory.count == 0 {
            return
        } else {
            let visitID = uploadingInventory[0].visitId
            let visitDAte = uploadingInventory[0].visitDate
            let zoneName = uploadingInventory[0].zoneName
            let shipName = uploadingInventory[0].shipName
            let retailerName = uploadingInventory[0].retailerName
            let myUrl = uploadingInventory[0].uploadingInventory
            
            let fullUrl :String = CommonClass.baseURL + "uploadReport"
            
            let params = ["visit_id":visitID,
                          "visit_date":visitDAte,
                          "zone_name":zoneName,
                          "retailer_name":retailerName,
                          "ship_name":shipName] as [String : AnyObject]
            print(params)
        
            self.multipartDataService(url: fullUrl, data: myUrl!, withName: "file_name", parameters: params, success: { (response) in
            if let response1 = response as? [String: Any] {
                if let status = response1["status"] as? Bool,
                    status == true {
                    DataBaseHelper.shareInstance.deleteUploadingInventory()
                    let message = response1["message"] as? String
                    notification(message: message!)
                }
            }
        }) { (error) in
            print(error)
        }
    }
    }
    
    
    class func multipartDataService(url:String, data:Any, withName:String, header:[String:String]? = CommonClass.apiKeyDict, parameters:[String:Any]?, success: @escaping (Any)-> (), failure: @escaping (Error)->()) -> Void
    {
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append((data as! URL), withName: withName)
            for (key, value) in parameters! {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:URL(string: url)!, headers:CommonClass.apiKeyDict)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                upload.responseData { response in
                    upload.responseJSON {(response) -> Void in
                        switch(response.result){
                        case .success(let value):
                            if let dict = value as? [String:Any]{
                                success(dict)
                            }
                            else{
                                let error = NSError(domain: "Error", code: 201, userInfo: ["Debugs info":"Response is not on json from"])
                                failure(error as Error)
                            }
                        case .failure(let error):
                            print(error)
                            failure(error)
                        }
                    }
                }
            case .failure(let error):
                print(error)
                failure(error)
            }
        }
    }
}
