//
//  ViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 22/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import MMDrawController

class ViewController: MMDrawerViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Init by storyboard identifier
        
        if UserDefaults.standard.object(forKey: "userid") != nil {
            super.setMainWith(identifier: "Home")
          super.setLeftWith(identifier: "Member", mode: .frontWidthRate(r: 1.0))
        }
        else {
            super.setMainWith(identifier: "login")
            self.navigationController?.isNavigationBarHidden = true
        }
        }
        
        //Init by Code
       // let story = UIStoryboard.init(name: "Main", bundle: nil)
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

