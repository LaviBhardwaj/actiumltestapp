//
//  DataBaseHelper.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 05/12/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DataBaseHelper{
    
    static var shareInstance = DataBaseHelper()
    let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    
    func save(object:[String:String]) {
        let profile = NSEntityDescription.insertNewObject(forEntityName: "Profile", into: context!) as! Profile
        profile.image = object["image"]
        profile.firstName = object["firstName"]
        profile.lastName = object["lastName"]
        profile.mobile = object["mobile"]
        profile.email = object["email"]
        profile.brandName = object["brandName"]
        do {
            try context?.save()
        }catch {
            print("data is not save")
        }
    }
    func getProfileData() -> [Profile] {
        var profile = [Profile]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Profile")
        do{
            profile = try context?.fetch(fetchRequest) as! [Profile]
            return profile
        }catch{
            print("Can't get data")
        }
        return []
    }
    
    func deleteProfileData() {
        var profile = getProfileData()
        for contextt in profile {
            context?.delete(contextt)
            profile.removeAll()
            do{
                try context?.save()
                print("successfully profile deleted")
            }catch{
                print("Profile data not deleted")
            }
        }
    }
    
    func saveBrand(object:[String:String]) {
        let brand = NSEntityDescription.insertNewObject(forEntityName: "Brand", into: context!) as!  Brand
        brand.imageStr = object["imageString"]
        brand.id = object["BrandId"]
        do{
            try context?.save()
        }catch {
            print("data is not save")
        }
    }
    
    func getBrandData() -> [Brand] {
        var brand = [Brand]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Brand")
        do{
            brand = try context?.fetch(fetchRequest) as! [Brand]
        }catch{
            print("can't get data")
        }
        return brand
    }
    
    func deleteBrandData() {
        var brand = getBrandData()
        for contextt in brand {
            context?.delete(contextt)
            brand.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
    
    func saveZone(object:[String:String]) {
        let zone = NSEntityDescription.insertNewObject(forEntityName: "Zone", into: context!) as! Zone
        zone.image = object["imageString"]
        zone.name = object["zoneName"]
        zone.id = object["zoneId"]
        do{
            try context?.save()
            print("ZoneSaved")
        }catch {
            print("data is not save")
        }
    }

    func getZoneData() -> [Zone] {
        var zone = [Zone]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Zone")
        do{
            zone = try context?.fetch(fetchRequest) as! [Zone]
            print("DataFetched")
        }catch{
            print("can't get data")
        }
        return zone
    }
    
    func deleteZoneData() {
        var zone = getZoneData()
        for contextt in zone {
            context?.delete(contextt)
            zone.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
    func savePort(object:[String:String]) {
        let port = NSEntityDescription.insertNewObject(forEntityName: "Port", into: context!) as! Port
        port.imageStr = object["imageString"]
        port.portId = object["portId"]
        port.portName = object["portName"]
        
        do{
            try context?.save()
        }catch{
            print("data is not save")
        }
    }
    
    func getPortData() -> [Port] {
        var port = [Port]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Port")
        do{
            port = try context?.fetch(fetchRequest) as! [Port]
        }catch{
            print("can't get data")
        }
        return port
    }
    func deletePortData() {
        var port = getPortData()
        for contextt in port {
            context?.delete(contextt)
            port.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
    func saveRetailer(object:[String:String]) {
        let retailer = NSEntityDescription.insertNewObject(forEntityName: "Retailer", into: context!) as! Retailer
        retailer.imageStr = object["imageString"]
        retailer.id = object["id"]
        retailer.name = object["retailerName"]
        do{
            try context?.save()
        }catch{
            print("data is not save")
        }
    }
    
    func getRetailerData() -> [Retailer] {
        var retailer = [Retailer]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Retailer")
        do{
            retailer = try context?.fetch(fetchRequest) as! [Retailer]
        }catch{
            print("can't get data")
        }
        return retailer
    }
    func deleteRetailerData() {
        var retailer = getRetailerData()
        for contextt in retailer {
            context?.delete(contextt)
            retailer.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
    
    func saveShip(object:[String:String]) {
        let ship = NSEntityDescription.insertNewObject(forEntityName: "Ship", into: context!) as! Ship
        ship.id = object["id"]
        ship.shipName = object["shipName"]
        do{
            try context?.save()
            print("data saved")
        }catch{
            print("data is not save")
        }
    }
    
    func getShipData() -> [Ship] {
        var ship = [Ship]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Ship")
        do{
            ship = try context?.fetch(fetchRequest) as! [Ship]
        }catch{
            print("can't get data")
        }
        return ship
    }
    func deleteShipData() {
        var ship = getShipData()
        for contextt in ship {
            context?.delete(contextt)
            ship.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
    
    func saveDate(object:[String:String]) {
        let dates = NSEntityDescription.insertNewObject(forEntityName: "Dates", into: context!) as! Dates
        dates.dates = object["date"]
        dates.visitId = object["visitId"]
        dates.visitRound = object["VisitRound"]
        do{
            try context?.save()
            print("data saved")
        }catch{
            print("data is not save")
        }
    }
    
    func getDateData() -> [Dates] {
        var dates = [Dates]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Dates")
        do{
            dates = try context?.fetch(fetchRequest) as! [Dates]
        }catch{
            print("can't get data")
        }
        return dates
    }
    func deleteDateData() {
        var dates = getDateData()
        for contextt in dates {
            context?.delete(contextt)
            dates.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
    
    func saveMultipleImages(object:[String:String]) {
        let multipleImage = NSEntityDescription.insertNewObject(forEntityName: "MultipleImages", into: context!) as! MultipleImages
        multipleImage.imageStr = object["Images"]
        do{
            try context?.save()
            print("data saved")
        }catch{
            print("data is not save")
        }
    }
    
    func getMultipleImages() -> [MultipleImages] {
        var multipleImage = [MultipleImages]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "MultipleImages")
        do{
            multipleImage = try context?.fetch(fetchRequest) as! [MultipleImages]
            print("dataFetched")
        }catch{
            print("can't get data")
        }
        return multipleImage
    }
    
    func deleteMultipleImages() {
        var multipleImage = getMultipleImages()
        for contextt in multipleImage {
            context?.delete(contextt)
            multipleImage.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
    
    func saveVideo(object:[String:String]) {
        let videoList = NSEntityDescription.insertNewObject(forEntityName: "VideoList", into: context!) as! VideoList
        videoList.video = object["VideoString"]
        do{
            try context?.save()
            print("data saved")
        }catch{
            print("data is not save")
        }
    }
    
    func getVideoList() -> [VideoList] {
        var videoList = [VideoList]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "VideoList")
        do{
            videoList = try context?.fetch(fetchRequest) as! [VideoList]
            print("dataFetched")
        }catch{
            print("can't get data")
        }
        return videoList
    }
    
    func deleteVideoList() {
        var videoList = getVideoList()
        for contextt in videoList {
            context?.delete(contextt)
            videoList.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
    func saveReport(object:[String:String]) {
        let report = NSEntityDescription.insertNewObject(forEntityName: "ReportList", into: context!) as! ReportList
        report.document = object["ReportPath"]
        do{
            try context?.save()
            print("data saved")
        }catch{
            print("data is not save")
        }
    }
    
    func getReportList() -> [ReportList] {
        var report = [ReportList]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ReportList")
        do{
            report = try context?.fetch(fetchRequest) as! [ReportList]
            print("dataFetched")
        }catch{
            print("can't get data")
        }
        return report
    }
    
    func deleteReportList() {
        var report = getReportList()
        for contextt in report {
            context?.delete(contextt)
            report.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
    func saveInventory(object:[String:String]) {
        let inventory = NSEntityDescription.insertNewObject(forEntityName: "InventoryList", into: context!) as! InventoryList
        inventory.document = object["InventoryPath"]
        do{
            try context?.save()
            print("data saved")
        }catch{
            print("data is not save")
        }
    }
    
    func getInventoryList() -> [InventoryList] {
        var inventory = [InventoryList]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "InventoryList")
        do{
            inventory = try context?.fetch(fetchRequest) as! [InventoryList]
            print("dataFetched")
        }catch{
            print("can't get data")
        }
        return inventory
    }
    
    func deleteInventoryList() {
        var inventory = getInventoryList()
        for contextt in inventory {
            context?.delete(contextt)
            inventory.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
    
    func saveUploadingImage(object:[String:String]) {
        let uploadingImage = NSEntityDescription.insertNewObject(forEntityName: "UploadingImages", into: context!) as! UploadingImages
        
        uploadingImage.uploadingImage = object["UploadingImages"]
        uploadingImage.visitId = object["VisitId"]
        uploadingImage.visitDate = object["Visitdate"]
        uploadingImage.zoneName = object["ZoneName"]
        uploadingImage.shipName = object["ShipName"]
        uploadingImage.retailername = object["RetailerName"]
        do{
            try context?.save()
            print("data saved")
        }catch{
            print("data is not save")
        }
    }
    
    func getUploadingImages() -> [UploadingImages] {
        var uploadingImage = [UploadingImages]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "UploadingImages")
        do{
            uploadingImage = try context?.fetch(fetchRequest) as! [UploadingImages]
            print("dataFetched")
        }catch{
            print("can't get data")
        }
        return uploadingImage
    }
    
    func deleteUploadingImages() {
        var uploadingImage = getUploadingImages()
        for contextt in uploadingImage {
            context?.delete(contextt)
            uploadingImage.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
    
    func saveUploadingVideo(object:[String:Any]) {
        let videoUrl = NSEntityDescription.insertNewObject(forEntityName: "UplodingVideo", into: context!) as! UplodingVideo
        videoUrl.videoUrl = object["UrlVideo"] as? URL
        videoUrl.visitId = object["VisitId"] as? String
        videoUrl.visitdate = object["VisitDate"] as? String
        videoUrl.zoneName = object["ZoneName"] as? String
        videoUrl.shipName = object["ShipName"] as? String
        videoUrl.retailerName = object["RetailerName"] as? String
        do{
            try context?.save()
            print("data saved")
        }catch{
            print("data is not save")
        }
    }
    
    func getUploadingVideo() -> [UplodingVideo] {
        var videoUrl = [UplodingVideo]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "UplodingVideo")
        do{
            videoUrl = try context?.fetch(fetchRequest) as! [UplodingVideo]
            print("dataFetched")
        }catch{
            print("can't get data")
        }
        return videoUrl
    }
    
    func deleteUploadingVideo() {
        var videoUrl = getUploadingVideo()
        for contextt in videoUrl {
            context?.delete(contextt)
            videoUrl.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
    
    func saveUploadingReport(object:[String:Any]) {
        let uploadingReport = NSEntityDescription.insertNewObject(forEntityName: "UploadingReport", into: context!) as! UploadingReport
        uploadingReport.uploadingReport = object["UploadingReport"] as? URL
        uploadingReport.visitId = object["VisitId"] as? String
        uploadingReport.visitDate = object["Visitdate"] as? String
        uploadingReport.zoneName = object["ZoneName"] as? String
        uploadingReport.shipName = object["ShipName"] as? String
        uploadingReport.retailerName = object["RetailerName"] as? String
        do{
            try context?.save()
            print("data saved")
        }catch{
            print("data is not save")
        }
    }
    
    func getUploadingReport() -> [UploadingReport] {
        var uploadingReport = [UploadingReport]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "UploadingReport")
        do{
            uploadingReport = try context?.fetch(fetchRequest) as! [UploadingReport]
            print("dataFetched")
        }catch{
            print("can't get data")
        }
        return uploadingReport
    }
    
    func deleteUploadingReport() {
        var uploadingReport = getUploadingReport()
        for contextt in uploadingReport {
            context?.delete(contextt)
            uploadingReport.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
    
    func saveUploadingInventory(object:[String:Any]) {
        let uploadingInventory = NSEntityDescription.insertNewObject(forEntityName: "UploadingInventory", into: context!) as! UploadingInventory
        uploadingInventory.uploadingInventory = object["UploadingInventory"] as? URL
        uploadingInventory.visitId = object["VisitId"] as? String
        uploadingInventory.visitDate = object["Visitdate"] as? String
        uploadingInventory.zoneName = object["ZoneName"] as? String
        uploadingInventory.shipName = object["ShipName"] as? String
        uploadingInventory.retailerName = object["RetailerName"] as? String
        do{
            try context?.save()
            print("data saved")
        }catch{
            print("data is not save")
        }
    }
    
    func getUploadingInventory() -> [UploadingInventory] {
        var uploadingInventory = [UploadingInventory]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "UploadingInventory")
        do{
            uploadingInventory = try context?.fetch(fetchRequest) as! [UploadingInventory]
            print("dataFetched")
        }catch{
            print("can't get data")
        }
        return uploadingInventory
    }
    
    func deleteUploadingInventory() {
        var uploadingInventory = getUploadingInventory()
        for contextt in uploadingInventory {
            context?.delete(contextt)
            uploadingInventory.removeAll()
            do{
                try context?.save()
                print("successfully deleted")
            }catch{
                print("data not deleted")
            }
        }
    }
}

