//
//  CommonClass.swift
//  ActiumApp
//
//  Created by Rahul Mishra on 22/11/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import Foundation
import Reachability
import UserNotifications

class CommonClass: NSObject {
    
    static let baseURL = "http://3.0.78.175/actium-dev/api/user/"
    static var profilePicture = UIImageView()
    var reachability: Reachability!
    static var apiKeyDict = ["X-API-KEY":"86451504487681cb3012f4132da78e64"]
    
    // MARK: - Loaders
    class func showLoader() {
        if let window = UIApplication.shared.windows.last {
            MBProgressHUD.showAdded(to: window, animated: true)
            
        }
    }
    
    class func hideLoader() {
        if let window = UIApplication.shared.windows.last {
            MBProgressHUD.hide(for: window, animated: true)
        }
    }
    
    class func delegate() {
        func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
            
            //displaying the ios local notification when app is in foreground
            completionHandler([.alert, .badge, .sound])
        }
    }
    
    //get data from server with param
    
    class func getDataFromServer(_ methodName: String, parameter : [String: AnyObject], authentication : Bool , methodType :String ,completion: @escaping (_ result: AnyObject) -> Void) {
        
        let fullUrl = baseURL + methodName
        if authentication {
            var strName : String!
            var strPass : String!
            
            strName = UserDefaults.standard.object(forKey: "email") as! String
            strPass = UserDefaults.standard.object(forKey: "password")as! String
            
            var header = [String:String]()
            
            header["username"] = strName
            header["password"] = strPass
            
            let loginString = NSString(format:"%@:%@",strName,strPass)
            let loginData: Data = loginString.data(using: String.Encoding.utf8.rawValue)!
            let authValue=NSString(format: "Basic %@", loginData.base64EncodedString(options: []))
            header["Authorization"] = authValue as String
            
            Alamofire.request(fullUrl, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: header).validate(statusCode: 200..<501).responseJSON(completionHandler: { (response) in
                print(fullUrl)
                guard response.result.error == nil else
                {
                    print(response.result.error!)
                    completion(response.result.error! as AnyObject)
                    return
                }
                completion(response.result.value! as AnyObject)
            })
            
        }
        else
        {
            let header = ["X-API-KEY":"86451504487681cb3012f4132da78e64"]
            Alamofire.request(fullUrl, method: .post, parameters: parameter, encoding:URLEncoding.default, headers: header).validate(statusCode: 200..<501).responseJSON(completionHandler: { (response) in
                guard response.result.error == nil else
                {
                    print(response.result.error!)
                    completion(response.result.error! as AnyObject)
                    return
                }
                completion(response.result.value! as AnyObject)
            })
        }
    }
    class func rootViewController() -> UIViewController
    {
        return (UIApplication.shared.keyWindow?.rootViewController)!
    }
    
    // MARK: - Get topmost view controller
    
    class func topMostViewController(rootViewController: UIViewController) -> UIViewController?
    {
        if let navigationController = rootViewController as? UINavigationController
        {
            return topMostViewController(rootViewController: navigationController.visibleViewController!)
        }
        
        if let tabBarController = rootViewController as? UITabBarController
        {
            if let selectedTabBarController = tabBarController.selectedViewController
            {
                return topMostViewController(rootViewController: selectedTabBarController)
            }
        }
        
        if let presentedViewController = rootViewController.presentedViewController
        {
            return topMostViewController(rootViewController: presentedViewController)
        }
        return rootViewController
    }
    
    // MARK: Alert methods
    
    class func showAlert(_ message: String, okButtonTitle: String? = nil, target: UIViewController? = nil) {
        
        let topViewController: UIViewController? = CommonClass.topMostViewController(rootViewController: CommonClass.rootViewController())
        
        if let _ = topViewController {
            let alert = UIAlertController(title:"ActiumApp", message: message, preferredStyle: UIAlertControllerStyle.alert);
            let okBtnTitle = "OK"
            let okAction = UIAlertAction(title:okBtnTitle, style: UIAlertActionStyle.default, handler: nil);
            
            alert.addAction(okAction);
            if UIApplication.shared.applicationState != .background{
                topViewController?.present(alert, animated:true, completion:nil);
            }
        }
    }
    
    class NetworkManager: NSObject {
        
        var reachability: Reachability!
        
        // Create a singleton instance
        static let sharedInstance: NetworkManager = { return NetworkManager() }()
        
        
        override init() {
            super.init()
            
            // Initialise reachability
            reachability = Reachability()!
            
            // Register an observer for the network status
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(networkStatusChanged(_:)),
                name: .reachabilityChanged,
                object: reachability
            )
            do {
                // Start the network status notifier
                try reachability.startNotifier()
            } catch {
                print("Unable to start notifier")
            }
        }
        
        @objc func networkStatusChanged(_ notification: Notification) {
            // Do something globally here!
        }
        
        static func stopNotifier() -> Void {
            do {
                // Stop the network status notifier
                try (NetworkManager.sharedInstance.reachability).startNotifier()
            } catch {
                print("Error stopping notifier")
            }
        }
        
        // Network is reachable
        static func isReachable(completed: @escaping (NetworkManager) -> Void) {
            if (NetworkManager.sharedInstance.reachability).connection != .none {
                completed(NetworkManager.sharedInstance)
            }
        }
        
        // Network is unreachable
        static func isUnreachable(completed: @escaping (NetworkManager) -> Void) {
            if (NetworkManager.sharedInstance.reachability).connection == .none {
                completed(NetworkManager.sharedInstance)
            }
        }
        
        // Network is reachable via WWAN/Cellular
        static func isReachableViaWWAN(completed: @escaping (NetworkManager) -> Void) {
            if (NetworkManager.sharedInstance.reachability).connection == .cellular {
                completed(NetworkManager.sharedInstance)
            }
        }
        
        // Network is reachable via WiFi
        static func isReachableViaWiFi(completed: @escaping (NetworkManager) -> Void) {
            if (NetworkManager.sharedInstance.reachability).connection == .wifi {
                completed(NetworkManager.sharedInstance)
            }
        }
    }
    
}
