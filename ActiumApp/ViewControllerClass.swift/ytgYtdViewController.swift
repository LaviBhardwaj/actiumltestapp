//
//  ytgYtdViewController.swift
//  ActiumApp
//
//  Created by Rahul Mishra on 17/12/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit


class ytgYtdViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var ytgLbl: UILabel!
    @IBOutlet weak var ytdLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var zoneId = String()
    var brandId = String()
    var retailerName = String()
    var portName = String()
    var shipName = String()
    var ytdDateList = [String]()
    var ytgDateList = [String]()
    var currentDate = String()
    

    //MARK: - View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        currentDate = formatter.string(from: date)
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.ytdList()
            //self.ytgList()
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
                let alertController = UIAlertController(title: "ActiumApp", message: "No Internet Connection", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        
        let nib = UINib(nibName: "ytgYtdTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "cell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBActions
    @IBAction func backBarBtn(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Apis
    func ytdList() {
        
        let params = ["zone_name":zoneId,
                      "brand_id":UserDefaults.standard.string(forKey: "brandId"),
                      "retailer_name":retailerName,
                      "port_name":portName,
                      "ship_name":shipName,
                      "training_date":currentDate]
        
        CommonClass.getDataFromServer("ytdList", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") { result -> Void in
            print("YTD:--\(result)")
            if let dictResponse = result as? [String: Any] {
                if let status = dictResponse["status"] as? Bool,
                    status == true {
                    guard let data = dictResponse["data"] as? [[String:Any]] else {
                        return
                    }
                    for i in 0..<data.count {
                        guard let dateStr = data[i]["date"] as? String else {
                            return
                        }
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        guard let date = dateFormatter.date(from: dateStr) else {
                            fatalError()
                        }
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd-MMM-yyyy"
                        let str = formatter.string(from: date)
                        self.ytdDateList.append(str)
                        }
                    }
                        self.ytgList()
                //self.ytdLbl.text = "YTD(\(self.ytdDateList.count))"
                //self.tableView.reloadData()
                }
            }
        }
    
    func ytgList() {
        
        let params = ["zone_name":zoneId,
                      "brand_id":UserDefaults.standard.string(forKey: "brandId"),
                      "retailer_name":retailerName,
                      "port_name":portName,
                      "ship_name":shipName,
                      "training_date":currentDate]
        
        CommonClass.getDataFromServer("ytgList", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") { result -> Void in
            print("YTG:--\(result)")
            if let dictResponse = result as? [String: Any] {
                if let status = dictResponse["status"] as? Bool,
                    status == true {
                    guard let data = dictResponse["data"] as? [[String:Any]] else {
                        return
                    }
                    for i in 0..<data.count {
                        guard let dateStr = data[i]["date"] as? String else {
                            return
                        }
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        guard let date = dateFormatter.date(from: dateStr) else {
                            fatalError()
                        }
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd-MMM-yyyy"
                        let str = formatter.string(from: date)
                        self.ytgDateList.append(str)
                    }
                }
                self.ytdLbl.text = "YTD(\(self.ytdDateList.count))"
                self.ytgLbl.text = "YTG(\(self.ytgDateList.count))"
                self.tableView.reloadData()
            }
        }
    }
    }
//MARK: - Table View Method
extension ytgYtdViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if ytgDateList.count > ytdDateList.count {
                return ytgDateList.count
            }
             else {
                return ytdDateList.count
    }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if ytdDateList.count < ytgDateList.count {
            for i in ytdDateList.count...ytgDateList.count {
                    ytdDateList.insert("", at: i)
            }
         } else if ytdDateList.count > ytgDateList.count {
            for i in ytgDateList.count...ytdDateList.count {
                    ytgDateList.insert("", at: i)
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ytgYtdTableViewCell
            cell?.ytdLbl.text = ytdDateList[indexPath.row]
            cell?.ytgLbl.text = ytgDateList[indexPath.row]
        return cell!
    }
}



