//
//  VideoListViewController.swift
//  ActiumApp
//
//  Created by Rahul Mishra on 28/11/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Photos
import Alamofire
import UserNotifications

class VideoListViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addVideoBtn: UIButton!
    
    // MARK: - Properties
    var visitID = String()
    var visitDAte = String()
    var zoneName = String()
    var retailerName = String()
    var shipName = String()
    var videos = [String]()
    internal var imageEdited: UIImage!
    internal var urlVideo: URL!
    internal var netWorkStatus = Bool()
    var videoList = [VideoList]()
    var uploadingVideoSave = Bool()
    var videoId = [String]()
    var content = UNMutableNotificationContent()
    
    // MARK: - View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UserDefaults.standard.string(forKey: "userType") == "User" {
            addVideoBtn.isHidden = false
        }
        else{
            addVideoBtn.isHidden = true
        }
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.netWorkStatus = true
            self.videoUrl()
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.netWorkStatus = false
            if self.visitID == UserDefaults.standard.string(forKey: "VisitIdTemp") {
                self.videoList = DataBaseHelper.shareInstance.getVideoList()
            }
            else {
                let alertController = UIAlertController(title: "ActiumApp", message: "No Internet Connection", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        let nib = UINib(nibName: "VideoListTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "VideoListTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            DataSyncManager.uploadSavedImages()
            DataSyncManager.uploadSavedVideo()
            DataSyncManager.uploadSavedReport()
            DataSyncManager.uploadSavedInventory()
            UNUserNotificationCenter.current().delegate = self
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if uploadingVideoSave == true {
            CommonClass.showAlert("No Internet Connection Data will Sync When Internet Appeared")
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func backBarBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addVideoBtnActn(_ sender: UIButton) {
        showActionSheet()
    }
    
    
    // MARK: - APis
    func videoUrl() {
        
        CommonClass.showLoader()
        let params = ["visit_id":visitID,
                      "visit_date":visitDAte]
        
        CommonClass.getDataFromServer("videoList", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") {  result  -> Void in
            print(result)
            CommonClass.hideLoader()
            if let dictResponse = result as? [String: Any] {
                if let status = dictResponse["status"] as? Bool,
                    status == true {
                    self.videoId.removeAll()
                    self.videos.removeAll()
                    DataBaseHelper.shareInstance.deleteVideoList()
                    guard let data = dictResponse["data"] as? [[String: Any]] else {
                        return
                    }
                    for i in 0..<data.count {
                        guard let url = data[i]["video_path"] as? String else {
                            return
                        }
                        guard let id = data[i]["id"] as? String else {
                            return
                        }
                        let videoPath = url.replacingOccurrences(of: " ", with: "%20")
                        self.videoId.append(id)
                        self.videos.append(videoPath)
                        let dict = ["VideoString":videoPath]
                        DataBaseHelper.shareInstance.saveVideo(object: dict)
                    }
                    self.tableView.reloadData()
                }
                else {
                    self.tableView.reloadData()
                    let message = dictResponse["message"] as? String
                    let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                        print("You've pressed cancel")
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK: - APIS
    private func uploadVideo() {
        let fullUrl :String = CommonClass.baseURL + "uploadVideo"
        let params = ["visit_id":visitID,
                      "visit_date":visitDAte,
                      "zone_name":zoneName,
                      "retailer_name":retailerName,
                      "ship_name":shipName] as [String : AnyObject]
        CommonClass.showLoader()
        self.multipartVideoService(url: fullUrl, data: self.urlVideo, withName: "video", parameters: params, success: { (response) in
            CommonClass.hideLoader()
            if let response1 = response as? [String: Any] {
                if let status = response1["status"] as? Bool,
                    status == true {
                    DataBaseHelper.shareInstance.deleteUploadingVideo()
                    let message = response1["message"] as? String
                    
                    self.content.title = "Message"
                    self.content.body = message!
                    self.content.badge = 1
                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
                    
                    //getting the notification request
                    let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: self.content, trigger: trigger)
                    
                    UNUserNotificationCenter.current().delegate = self
                    
                    //adding the notification to notification center
                    UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                    
                    
                    let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                        self.videos.removeAll()
                        self.videoUrl()
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
                else {
                    let message = response1["message"] as? String
                    let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }) { (error) in
            print(error)
        }
    }
    
    func deleteVideo(videoName: String, videoId: String) {
        let params = ["video_name":videoName,
                      "videoChk":videoId]
        print(params)
        CommonClass.showLoader()
        CommonClass.getDataFromServer("deleteVideo", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") {  result  -> Void in
            print(result)
            if let error = result as? Error {
                CommonClass.hideLoader()
                print(error.localizedDescription)
                CommonClass.showAlert("Something went wrong, Please try again")
            } else {
                if let dictresponse = result as? [String:Any] {
                    CommonClass.hideLoader()
                    if let status = dictresponse["status"] as? Bool,
                        status == true {
                        self.videos.removeAll()
                        self.videoId.removeAll()
                        self.videoUrl()
                        if let message = dictresponse["message"] as? String {
                            CommonClass.showAlert(message)
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Functions
    func showActionSheet()
    {
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title:  "Cancel", style: .cancel) { action -> Void in
            // self.dismiss(animated: true, completion: nil)
        }
        actionSheetController.addAction(cancelActionButton)
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: "Camera", style: .default) { action -> Void in
            self.openCamera()
        }
        actionSheetController.addAction(cameraActionButton)
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: "Gallery", style: .default) { action -> Void in
            self.openGallery()
        }
        actionSheetController.addAction(galleryActionButton)
        self.present(actionSheetController , animated: true, completion: nil)
    }
    
}

// MARK: - Table View Methods
extension VideoListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if netWorkStatus == true {
            return videos.count
        } else {
            return videoList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoListTableViewCell", for: indexPath) as! VideoListTableViewCell
        cell.listIconImage.image = #imageLiteral(resourceName: "play-button")
        if netWorkStatus == true {
            let url = URL(string: videos[indexPath.row])
            let fileName =  (url)?.lastPathComponent
            cell.lblVideoPath.text = fileName
        } else {
            let url = URL(string: videoList[indexPath.row].video!)
            let fileName =  (url)?.lastPathComponent
            cell.lblVideoPath.text = fileName
        }
        if UserDefaults.standard.string(forKey: "userType") == "User" {
            cell.deleteBtn.isHidden = false
            cell.deleteBtn.isEnabled = true
        }
        else{
            cell.deleteBtn.isHidden = true
            cell.deleteBtn.isEnabled = false
        }
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteRow), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if netWorkStatus == true {
            if let videoURL = URL(string: videos[indexPath.row])
            {
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }
        }else {
            if let videoURL = URL(string: videoList[indexPath.row].video!)
            {
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }
        }
    }
    
    @objc func deleteRow(sender: UIButton){
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            let index = sender.tag
            let alertController = UIAlertController(title: "Message", message: "Do you want to delete this item?", preferredStyle: .alert)
            let actionCancel = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
            }
            let actionDelete = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction) in
                let url = URL(string: self.videos[index])
                let fileName =  (url)?.lastPathComponent
                let videoId = self.videoId[index]
                self.deleteVideo(videoName: fileName!, videoId: videoId)
            }
            alertController.addAction(actionCancel)
            alertController.addAction(actionDelete)
            self.present(alertController, animated: true, completion: nil)
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            CommonClass.showAlert("No Internet Connection")
        }
    }
}
    
extension VideoListViewController {
    
    // MARK: - Functions for Taking Video
    private func openGallery()
    {
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
            case .authorized: self.takeGalleryPhoto()
            case .notDetermined: self.takeGalleryPhoto()
            default: self.alertPromptToAllowCameraAccessViaSettings()
            }
        }
    }
    
    private func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera)
        {
            let authStatus = AVCaptureDevice.authorizationStatus(for: .video)
            switch authStatus {
            case .authorized: takeCameraPhoto()
            case .notDetermined: takeCameraPhoto()
            default: alertPromptToAllowCameraAccessViaSettings()
            }
        }
    }
    
    private func takeCameraPhoto() {
        
        ImgPickerHandler.sharedHandler.getImageDict(allowsEditing:false, instance: self, isSourceCamera: true, rect: nil, mediaType: PickerMediaType.VideoOnly, completion:
            {[weak self]
                (dict, success) in
                if success {
                    if let videoURl = dict[UIImagePickerControllerMediaURL] as? URL{
                        self?.urlVideo = videoURl
                        
                        CommonClass.NetworkManager.isReachable { networkManagerInstance in
                            self?.uploadVideo()
                        }
                        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
                            let dict = ["UrlVideo":self?.urlVideo as Any,"VisitId":self?.visitID as Any,"VisitDate":self?.visitDAte as Any,"ZoneName":self?.zoneName as Any,"ShipName":self?.shipName as Any,"RetailerName":self?.retailerName as Any] as [String : Any]
                             
                            DataBaseHelper.shareInstance.saveUploadingVideo(object: dict)
                            self?.uploadingVideoSave = true
                        }
                    }}
                else {
                }
        })
    }
    
    private func takeGalleryPhoto() {
        
        ImgPickerHandler.sharedHandler.getImageDict(allowsEditing:false, instance: self, isSourceCamera: false, rect: nil, mediaType: PickerMediaType.VideoOnly, completion:
            {[weak self]
                (dict, success) in
                if success{
                    if let videoURl = dict[UIImagePickerControllerMediaURL] as? URL{
                        self?.urlVideo = videoURl
                        CommonClass.NetworkManager.isReachable { networkManagerInstance in
                            self?.uploadVideo()
                        }
                        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
                            let dict = ["UrlVideo":self?.urlVideo as Any,"VisitId":self?.visitID as Any,"VisitDate":self?.visitDAte as Any,"ZoneName":self?.zoneName as Any,"ShipName":self?.shipName as Any,"RetailerName":self?.retailerName as Any] as [String : Any]
                            
                            DataBaseHelper.shareInstance.saveUploadingVideo(object: dict)
                            self?.uploadingVideoSave = true
                        }
                    }} else {
                }
        })
    }

    private func alertPromptToAllowCameraAccessViaSettings() {
        
        showAlertWithAction(title: "Actium App", message: "Please grant permission to use the Camera/Gallery", style: .alert, actionTitles: ["Open Settings", "Cancel"], action: { (alert) in
            if let title = alert.title {
                switch title {
                case "Open Settings":
                    if let appSettingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(appSettingsURL, options: [:], completionHandler: { (isOpen) in
                            })
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                default:
                    break
                }
            }
        })
    }
    
    func showAlertWithAction(title: String?, message: String?, style: UIAlertControllerStyle, actionTitles:[String?], action:((UIAlertAction) -> Void)?) {
        
        showAlertWithActionWithCancel(title: title, message: message, style: style, actionTitles: actionTitles, showCancel: false, deleteTitle: nil, action: action)
    }
    
    func showAlertWithActionWithCancel(title: String?, message: String?, style: UIAlertControllerStyle, actionTitles:[String?], showCancel:Bool, deleteTitle: String? ,_ viewC: UIViewController? = nil, action:((UIAlertAction) -> Void)?) {
        
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        if deleteTitle != nil {
            let deleteAction = UIAlertAction(title: deleteTitle, style: .destructive, handler: action)
            alertController.addAction(deleteAction)
        }
        for (_, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: action)
            alertController.addAction(action)
        }
        
        if showCancel {
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
        }
        if let viewController = viewC {
            
            viewController.present(alertController, animated: true, completion: nil)
            
        } else {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    private func getThumbnailFrom(path: URL) -> UIImage? {
        do {
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            
            return thumbnail
            
        } catch let error {
            print(error)
            return nil
        }
    }
}
//MARK: - multipartDataService
extension VideoListViewController {
    
    func multipartVideoService(url:String, data:Any, withName:String, header:[String:String]? = CommonClass.apiKeyDict, parameters:[String:Any]?, success: @escaping (Any)-> (), failure: @escaping (Error)->()) -> Void
    {
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append((data as! URL), withName: withName)
            for (key, value) in parameters! {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:URL(string: url)!, headers:CommonClass.apiKeyDict)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                upload.responseData { response in
                    upload.responseJSON {(response) -> Void in
                        switch(response.result){
                        case .success(let value):
                            if let dict = value as? [String:Any]{
                                success(dict)
                            }
                            else{
                                let error = NSError(domain: "Error", code: 201, userInfo: ["Debugs info":"Response is not on json from"])
                                failure(error as Error)
                            }
                        case .failure(let error):
                            print(error)
                            failure(error)
                        }
                    }
                }
            case .failure(let error):
                print(error)
                failure(error)
            }
        }
    }
}
