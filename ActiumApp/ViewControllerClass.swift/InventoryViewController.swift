//
//  InventoryViewController.swift
//  ActiumApp
//
//  Created by Rahul Mishra on 29/11/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import Alamofire
import UserNotifications

class InventoryViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addInventoryBtn: UIButton!
    
    // MARK: - Properties
    var visitID = String()
    var visitDAte = String()
    var zoneName = String()
    var retailerName = String()
    var shipName = String()
    var filesArry = [String]()
    internal var myUrl : URL!
    var netWorkStatus = Bool()
    var inventory = [InventoryList]()
    var fileId = [String]()
    var content = UNMutableNotificationContent()

    // MARK: - Views Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.string(forKey: "userType") == "User" {
            addInventoryBtn.isHidden = false
        }
        else{
            addInventoryBtn.isHidden = true
        }
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.netWorkStatus = true
            self.inventoryList()
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.netWorkStatus = false
            if self.visitID == UserDefaults.standard.string(forKey: "VisitIdTemp") {
                self.inventory = DataBaseHelper.shareInstance.getInventoryList()
            }
            else {
                let alertController = UIAlertController(title: "ActiumApp", message: "No Internet Connection", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        
        let nib = UINib(nibName: "VideoListTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "VideoListTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            DataSyncManager.uploadSavedImages()
            DataSyncManager.uploadSavedVideo()
            DataSyncManager.uploadSavedReport()
            DataSyncManager.uploadSavedInventory()
            UNUserNotificationCenter.current().delegate = self
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    
    // MARK: - IBActions
    @IBAction func backBarBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addInventoryBtnActn(_ sender: UIButton) {
        documentPicker()
    }
    
    // MARK: - Functions
    func documentPicker() {
        //["public.text", "com.apple.iwork.pages.pages", "public.data"]
        //   let docum = UIDocumentPickerViewController(documentTypes: [.kuttye]
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.data"], in: .import )
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .pageSheet
        present(documentPicker, animated: true, completion: nil )
    }
    //MARK: - APIs
    func inventoryList() {
        
        let params = ["visit_id":visitID,
                      "visit_date":visitDAte]
        
        CommonClass.getDataFromServer("inventoryList", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") { result -> Void in
            if let dictResponse = result as? [String: Any] {
                if let status = dictResponse["status"] as? Bool,
                    status == true {
                    self.filesArry.removeAll()
                    self.fileId.removeAll()
                    DataBaseHelper.shareInstance.deleteInventoryList()
                    guard let data = dictResponse["data"] as? [[String: Any]] else {
                        return
                    }
                    for i in 0..<data.count {
                        guard let url = data[i]["file_path"] as? String else {
                            return
                        }
                        guard let id = data[i]["id"] as? String else {
                            return
                        }
                        let filePath = url.replacingOccurrences(of: " ", with: "%20")
                        self.fileId.append(id)
                        self.filesArry.append(filePath)
                        let dict = ["InventoryPath":filePath]
                        DataBaseHelper.shareInstance.saveInventory(object: dict)
                    }
                    self.tableView.reloadData()
                }
                else {
                    self.tableView.reloadData()
                    let message = dictResponse["message"] as? String
                    let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }

    private func uploadInventory() {
        
        let fullUrl :String = CommonClass.baseURL + "uploadInventory"
        
        let params = ["visit_id":visitID,
                      "visit_date":visitDAte,
                      "zone_name":zoneName,
                      "retailer_name":retailerName,
                      "ship_name":shipName] as [String : AnyObject]
        
        CommonClass.showLoader()
        self.multipartDocumentService(url: fullUrl, data: self.myUrl, withName: "file_name", parameters: params, success: { (response) in
            CommonClass.hideLoader()
            print(response)
            if let response1 = response as? [String: Any] {
                if let status = response1["status"] as? Bool,
                    status == true {
                    let message = response1["message"] as? String
                    
                    self.content.title = "Message"
                    self.content.body = message!
                    self.content.badge = 1
                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
                    
                    //getting the notification request
                    let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: self.content, trigger: trigger)
                    
                    UNUserNotificationCenter.current().delegate = self
                    
                    //adding the notification to notification center
                    UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                    
                    
                    let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                        self.filesArry.removeAll()
                        self.inventoryList()
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
                else {
                    let message = response1["message"] as? String
                    let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }) { (error) in
            print(error)
        }
    }
    
    func deletefile(fileName: String, fileId: String) {
        let params = ["inventory_name":fileName,
                      "invtChk":fileId]
        print(params)
        CommonClass.showLoader()
        CommonClass.getDataFromServer("deleteInventory", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") {  result  -> Void in
            print(result)
            if let error = result as? Error {
                CommonClass.hideLoader()
                print(error.localizedDescription)
                CommonClass.showAlert("Something went wrong, Please try again")
            } else {
                if let dictresponse = result as? [String:Any] {
                    CommonClass.hideLoader()
                    if let status = dictresponse["status"] as? Bool,
                        status == true {
                        self.filesArry.removeAll()
                        self.fileId.removeAll()
                        self.inventoryList()
                        if let message = dictresponse["message"] as? String {
                            CommonClass.showAlert(message)
                        }
                    }
                }
            }
        }
    }
}

// MARK: - Table View and Document Picker Method
extension InventoryViewController: UITableViewDelegate, UITableViewDataSource, UIDocumentPickerDelegate, UINavigationControllerDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if netWorkStatus == true {
            return filesArry.count
        } else {
            return inventory.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoListTableViewCell", for: indexPath) as? VideoListTableViewCell
        cell?.listIconImage.image = #imageLiteral(resourceName: "document")
        if netWorkStatus == true {
            let url = URL(string: filesArry[indexPath.row])
            let fileName =  (url)?.lastPathComponent
            cell?.lblVideoPath.text = fileName
        } else {
            let url = URL(string: inventory[indexPath.row].document!)
            let fileName =  (url)?.lastPathComponent
            cell?.lblVideoPath.text = fileName
        }
        if UserDefaults.standard.string(forKey: "userType") == "User" {
            cell?.deleteBtn.isHidden = false
            cell?.deleteBtn.isEnabled = true
        }
        else{
            cell?.deleteBtn.isHidden = true
            cell?.deleteBtn.isEnabled = false
        }
        
        cell?.deleteBtn.tag = indexPath.row
        cell?.deleteBtn.addTarget(self, action: #selector(deleteRow), for: .touchUpInside)
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let urlstring: String = filesArry[indexPath.row]
        UIApplication.shared.open(NSURL(string: urlstring)! as URL)
        
//        let story = UIStoryboard(name: "Main", bundle: nil)
//        let vc = story.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
//        if netWorkStatus == true {
//            vc?.webViewUrl = filesArry[indexPath.row]
//        } else {
//            vc?.webViewUrl = inventory[indexPath.row].document
//        }
//        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func deleteRow(sender: UIButton){
        
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            let index = sender.tag
            let alertController = UIAlertController(title: "Message", message: "Do you want to delete this item?", preferredStyle: .alert)
            let actionCancel = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
            }
            let actionDelete = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction) in
                let url = URL(string: self.filesArry[index])
                let Name =  (url)?.lastPathComponent
                let Id = self.fileId[index]
                self.deletefile(fileName: Name!, fileId: Id)
            }
            alertController.addAction(actionCancel)
            alertController.addAction(actionDelete)
            self.present(alertController, animated: true, completion: nil)
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            CommonClass.showAlert("No Internet Connection")
        }
    }
    
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        myUrl = url as URL
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.uploadInventory()
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            let dict = ["UploadingInventory":self.myUrl,"VisitId":self.visitID,"Visitdate":self.visitDAte,"ZoneName":self.zoneName,"ShipName":self.shipName,"RetailerName":self.retailerName] as [String : Any]
            DataBaseHelper.shareInstance.saveUploadingInventory(object: dict)
            CommonClass.showAlert("No Internet Connection Data will Sync When Internet Appeared")
        }
        print("import result : \(myUrl)")
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}

//MARK: Multiparts Data Service
extension InventoryViewController {
    
    func multipartDocumentService(url:String, data:Any, withName:String, header:[String:String]? = CommonClass.apiKeyDict, parameters:[String:Any]?, success: @escaping (Any)-> (), failure: @escaping (Error)->()) -> Void
    {
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append((data as! URL), withName: withName)
            for (key, value) in parameters! {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:URL(string: url)!, headers:CommonClass.apiKeyDict)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                upload.responseData { response in
                    upload.responseJSON {(response) -> Void in
                        switch(response.result){
                        case .success(let value):
                            if let dict = value as? [String:Any]{
                                success(dict)
                            }
                            else{
                                let error = NSError(domain: "Error", code: 201, userInfo: ["Debugs info":"Response is not on json from"])
                                failure(error as Error)
                            }
                        case .failure(let error):
                            print(error)
                            failure(error)
                        }
                    }
                }
            case .failure(let error):
                print(error)
                failure(error)
            }
        }
    }
}
