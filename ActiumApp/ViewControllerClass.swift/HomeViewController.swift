//
//  HomeViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 23/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import MMDrawController
import UserNotifications

class HomeViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    // MARK: - IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    var menubtntapped = false
    var arrImg = [String]()
    var brandId = [String]()
    var firstName = String()
    var lastNmae = String()
    var mobileNo = String()
    var emailId = String()
    var brand = [Brand]()
    var networkStatus = Bool()
    var userTypeId = String()
    var brandName = String()
    var brandNameArr = [String]()
    
    // MARK: - Views life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {didAllow, error in
        })
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.brandUrl()
            self.networkStatus = true
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.networkStatus = false
            self.brand = DataBaseHelper.shareInstance.getBrandData()
        }
        
        // MARK: - collection ViewCell Registration
        collectionView.register(UINib.init(nibName: "RetailerCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            DataSyncManager.uploadSavedImages()
            DataSyncManager.uploadSavedVideo()
            DataSyncManager.uploadSavedReport()
            DataSyncManager.uploadSavedInventory()
            UNUserNotificationCenter.current().delegate = self
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    
    // MARK: - IBAction
    @IBAction func smenuBtnActn(_ sender: UIBarButtonItem) {
        if let drawer = self.drawer() ,
            let manager = drawer.getManager(direction: .left){
            let value = !manager.isShow
            drawer.showLeftSlider(isShow: value)
        }
    }
    
    // MARK: -Apis
    func brandUrl() {
        
        let params = ["user_id":UserDefaults.standard.string(forKey: "userid"),
                      "type":UserDefaults.standard.string(forKey: "userTypeId")]
        CommonClass.showLoader()
        print(params)
        CommonClass.getDataFromServer("brandList", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") { dataa -> Void in
            print(dataa)
            if let error = dataa as? Error {
                CommonClass.hideLoader()
                print(error.localizedDescription)
                CommonClass.showAlert("Something went wrong, Please try again")
            } else {
                CommonClass.hideLoader()
                if let dictResponse = dataa as? [String : Any]{
                    if let dataArray = dictResponse["data"] as? [[String : Any]]{
                        DataBaseHelper.shareInstance.deleteBrandData()
                        for i in 0 ..< dataArray.count{
                            if let imgStr = dataArray[i]["brand_logo"] as? String {
                                let imgStr1 = imgStr.replacingOccurrences(of: " ", with: "%20")
                                self.arrImg.append(imgStr1)
                                let brandId = dataArray[i]["id"] as? String
                                self.brandId.append(brandId!)
                                let brandName = dataArray[i]["brand_name"] as? String
                                self.brandNameArr.append(brandName!)
                                let dict = ["imageString":imgStr1,"BrandId":brandId]
                                DataBaseHelper.shareInstance.saveBrand(object: dict as! [String : String])
                            }
                        }
                        self.collectionView.reloadData()
                    }
                }
            }
        }
     }
}

// MARK: - CollectionView Datasource and Delegate

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if networkStatus == true {
            return arrImg.count
        }
        else {
            return brand.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : RetailerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RetailerCell
        if networkStatus == true {
            if let url = NSURL(string: arrImg[indexPath.row]) {
                cell.retailImg?.af_setImage(withURL: url as URL, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (image) in
                    
                    let imageWithChangedColor = cell.retailImg.image?.withRenderingMode(.alwaysTemplate)
                    cell.retailImg.tintColor = .white
                    cell.retailImg.image = imageWithChangedColor
                }
            }
        } else {
            if let url = NSURL(string: brand[indexPath.row].imageStr!) {
                cell.retailImg?.af_setImage(withURL: url as URL, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (image) in
                    
                    let imageWithChangedColor = cell.retailImg.image?.withRenderingMode(.alwaysTemplate)
                    cell.retailImg.tintColor = .white
                    cell.retailImg.image = imageWithChangedColor
                }
            }
        }
        
        CommonClass.hideLoader()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc: SalesViewController = story.instantiateViewController(withIdentifier: "SalesViewController") as! SalesViewController
        if brandId.count == 0 {
            let id = brand[indexPath.row].id
            UserDefaults.standard.set(id, forKey: "brandId")
            UserDefaults.standard.set(id, forKey: "brandIdSaved")
        } else {
            let id = brandId[indexPath.row]
            UserDefaults.standard.set(id, forKey: "brandId")
            UserDefaults.standard.set(id, forKey: "brandIdTemp")
            UserDefaults.standard.set(brandNameArr[indexPath.row], forKey: "brandName")
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width / 2 - 5, height: UIScreen.main.bounds.size.width / 2 - 5)
    }
}
