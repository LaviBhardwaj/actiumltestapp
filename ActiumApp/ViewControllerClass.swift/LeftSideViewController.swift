//
//  LeftSideViewController.swift
//  ActiumApp
//
//  Created by Rahul Mishra on 30/11/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class LeftSideViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var slideTableView: UITableView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var emailIdLbl: UILabel!

    //MARK: - Properties
    var arrayLogoImg = [#imageLiteral(resourceName: "brand"), #imageLiteral(resourceName: "chat"), #imageLiteral(resourceName: "profile"),#imageLiteral(resourceName: "logout")]
    var arrayLogoName = [String]()
    var arrImg = [String]()
    var profileImage = String()
    var profileData = [String: Any]()
    var profile = [Profile]()
    var firstName = String()
    var lastName = String()
    var emailId = String()
    var mobileNo = String()
    var apiKeyDict = ["X-API-KEY":"86451504487681cb3012f4132da78e64"]
    
    
    //MARK: - View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.userProfileDetail()
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.profile = DataBaseHelper.shareInstance.getProfileData()
            self.setUpProfileOffline()
        }
        
        let nib = UINib(nibName: "HomeTableViewCell", bundle: nil)
        slideTableView.register(nib, forCellReuseIdentifier: "HomeTableViewCell")
        slideTableView.delegate = self
        slideTableView.dataSource = self
        
        profileImg.layer.borderWidth = 1
        profileImg.layer.borderColor = UIColor.lightGray.cgColor
        profileImg.layer.masksToBounds = false
        profileImg.layer.borderColor = UIColor.black.cgColor
        profileImg.layer.cornerRadius = profileImg.frame.height/2
        profileImg.clipsToBounds = true

         self.navigationController?.view.layoutSubviews()
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.userProfileDetail()
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.profile = DataBaseHelper.shareInstance.getProfileData()
            self.setUpProfileOffline()
        }
    }
    
    private func setUpProfileOffline() {
        if let emailId = self.profile[0].email {
            self.emailIdLbl.text = emailId
        }
        if let imageUrl = self.profile[0].image {
            if let url = NSURL(string: imageUrl) {
                self.profileImg.af_setImage(withURL: url as URL)
            }
        }
        else {
            self.profileImg.image = #imageLiteral(resourceName: "dummy_profile")
        }
    }
    
    //MARK:= APis
    func logOutMethod() {
        
        let url = URL(string: "http://3.0.78.175/actium-dev/api/user/logout")
        let params = ["userid":UserDefaults.standard.string(forKey: "userid")]
        
        Alamofire.request(url!, method: .post, parameters: params as Any as? Parameters,encoding: JSONEncoding.default, headers: apiKeyDict)
            .validate()
            .responseJSON(completionHandler: { (response) in
                
                switch response.result {
                case .success(let result):
                    UserDefaults.standard.removeObject(forKey: "userid")
                    UserDefaults.standard.removeObject(forKey: "userType")
                    UserDefaults.standard.removeObject(forKey: "retailer_name")
                    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
                    print(result)
                    
                case .failure(let error):
                    print(error)
            }
        })
    }
    
  func userProfileDetail() {
    
    let params = ["userid":UserDefaults.standard.string(forKey: "userid")]
    
    
    CommonClass.getDataFromServer("userProfileDetail", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") { result -> Void in
        print(result)
        if let dictResponse = result as? [String: Any] {
            if let status = dictResponse["status"] as? Bool,
                status == true {
                DataBaseHelper.shareInstance.deleteProfileData()
                guard let userData = dictResponse["userdata"] as? [String: Any] else {
                    return
                }
                self.profileData = userData
                if let Imageurl = userData["profile_image"] as? String {
                    self.profileImage = Imageurl
                }
                if let url = NSURL(string: self.profileImage) {
                    self.profileImg.af_setImage(withURL: url as URL)
                    CommonClass.profilePicture.af_setImage(withURL: url as URL)
                } else {
                    self.profileImg.image = #imageLiteral(resourceName: "dummy_profile")
                }
                if let firstname = self.profileData["first_name"] as? String {
                    self.firstName = firstname
                }
                if let lastname = self.profileData["last_name"] as? String {
                    self.lastName = lastname
                }
                if let mobileno = self.profileData["mobile"] as? String {
                    self.mobileNo = mobileno
                }
                if let email = self.profileData["email"] as? String {
                    self.emailId = email
                }
                self.emailIdLbl.text = self.emailId
                
                let dict = ["image":self.profileImage,"firstName":self.firstName,"lastName":self.lastName,"mobile":self.mobileNo,"email":self.emailId,"brandName":UserDefaults.standard.string(forKey: "retailerName") ?? ""]
                DataBaseHelper.shareInstance.save(object: dict )
                
            }
            else {
                let message = dictResponse["message"] as? String
                let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)

                let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                }
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
            self.slideTableView.reloadData()
        }
    }
  }
}

//MARK: - Table View Method
extension LeftSideViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayLogoImg.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: HomeTableViewCell = slideTableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        arrayLogoName = ["Brand", "Chat", "Profile", "LogOut"]
        cell.logoImage.image = arrayLogoImg[indexPath.row]
        cell.logoLabel.text = arrayLogoName[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if let d = self.drawer() {
            d.showLeftSlider(isShow: false)
            
            var identifier = ""
                identifier = "Home"
            
            if indexPath.row == 0 {
               
                d.setMain(identifier: identifier, config: { (vc) in
                    if let nav = vc as? UINavigationController {
                        nav.viewControllers.first?.title = "Rename Home"
                    }
                })
            }
            if indexPath.row == 2 {
                d.setMainWith(identifier: "Profile")
            }
             if indexPath.row == 3  {
                CommonClass.NetworkManager.isReachable { networkManagerInstance in
                    
                    let alertController = UIAlertController(title: "ActiumApp", message: "Do you want to LogOut?", preferredStyle: .alert)
                    let actionCancel = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
                    }
                    let actionDelete = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                        self.logOutMethod()
                    }
                    alertController.addAction(actionDelete)
                    alertController.addAction(actionCancel)
                    self.present(alertController, animated: true, completion: nil)
                    //self.logOutMethod()
                    //d.setMainWith(identifier: "login")
                }
                CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
                    CommonClass.showAlert("No Internet Connection")
                }
            }
        }
    }

}
