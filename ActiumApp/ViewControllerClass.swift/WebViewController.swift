//
//  WebViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 04/12/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var webView: UIWebView!
    
    //MARK: - Properties
    var webViewUrl : String!
    
    
    //MARK: - View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: webViewUrl)
        let request = NSURLRequest(url: url!)
        webView.loadRequest(request as URLRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBarBtn(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
