//
//  DeletePhotosController.swift
//  ActiumApp
//
//  Created by Rahul Mishra on 05/02/19.
//  Copyright © 2019 Techsaga Corporations. All rights reserved.
//

import UIKit
import AlamofireImage

class DeletePhotosController: UIViewController {

    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var visitId = String()
    var visitDate = String()
    var imageArr = [String]()
    var imageId = [String]()
    var multipleImage = [MultipleImages]()
    var netWorkStatus = Bool()
    
    //MARK: - View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.netWorkStatus = true
            self.multipleImages()
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.netWorkStatus = false
            if self.visitId == UserDefaults.standard.string(forKey: "VisitIdTemp") {
                self.multipleImage = DataBaseHelper.shareInstance.getMultipleImages()
            }
            else {
                let alertController = UIAlertController(title: "ActiumApp", message: "No Internet Connection", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        }
       
        let nib = UINib(nibName: "VideoListTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "VideoListTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Apis
    func multipleImages() {
        
        let params = ["visit_id":visitId,
                      "visit_date":visitDate]
        CommonClass.showLoader()
        CommonClass.getDataFromServer("multipleImageList", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") { result -> Void in
            print(result)
            CommonClass.hideLoader()
            if let dictResponse = result as? [String: Any] {
                if let status = dictResponse["status"] as? Bool,
                    status == true {
                    self.imageArr.removeAll()
                    self.imageId.removeAll()
                    DataBaseHelper.shareInstance.deleteMultipleImages()
                    guard let data = dictResponse["data"] as? [[String: Any]] else {
                        return
                    }
                    for i in 0 ..< data.count{
                        if let imagePath = data[i]["image_path"] as? String {
                            self.imageArr.append(imagePath)
                            let dict = ["Images":imagePath]
                            DataBaseHelper.shareInstance.saveMultipleImages(object: dict)
                        }
                        if let id = data[i]["id"] as? String {
                            self.imageId.append(id)
                        }
                    }
                    self.tableView.reloadData()
                }
                else {
                    self.tableView.reloadData()
                    let message = dictResponse["message"] as? String
                    let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                        print("You've pressed cancel")
                    }
                    alertController.addAction(action)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func deleteImages(imageName: String, imageId: String) {
        let params = ["image_name":imageName,
                      "imgChk":imageId]
        print(params)
        CommonClass.showLoader()
        CommonClass.getDataFromServer("deleteImage", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") {  result  -> Void in
            print(result)
            if let error = result as? Error {
                CommonClass.hideLoader()
                print(error.localizedDescription)
                CommonClass.showAlert("Something went wrong, Please try again")
            } else {
                if let dictresponse = result as? [String:Any] {
                    CommonClass.hideLoader()
                    if let status = dictresponse["status"] as? Bool,
                        status == true {
                        self.imageArr.removeAll()
                        self.imageId.removeAll()
                        self.multipleImages()
                        if let message = dictresponse["message"] as? String {
                            CommonClass.showAlert(message)
                        }
                    }
                }
            }
        }
    }
    
    
    //MARK: - IBActions
    @IBAction func backBarBtnActn(_ sender: UIButton) {
        UserDefaults.standard.set("isapicall", forKey: "isApiCall")
        self.navigationController?.popViewController(animated: true)
    }
}

extension DeletePhotosController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if netWorkStatus == true {
            return imageArr.count
        } else {
            return multipleImage.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoListTableViewCell", for: indexPath) as? VideoListTableViewCell
        if netWorkStatus == true {
            if let url = NSURL(string: imageArr[indexPath.row]) {
                cell?.listIconImage.af_setImage(withURL: url as URL)
            }
            let fileNameUrlurl = URL(string: imageArr[indexPath.row])
            let fileName =  (fileNameUrlurl)?.lastPathComponent
            cell?.lblVideoPath.text = fileName
        } else {
            if let url = NSURL(string: multipleImage[indexPath.row].imageStr!) {
                cell?.listIconImage.af_setImage(withURL: url as URL)
            }
            let fileNameUrlurl = URL(string: multipleImage[indexPath.row].imageStr!)
            let fileName =  (fileNameUrlurl)?.lastPathComponent
            cell?.lblVideoPath.text = fileName
        }
        
        cell?.deleteBtn.tag = indexPath.row
        cell?.deleteBtn.addTarget(self, action: #selector(deleteRow), for: .touchUpInside)
        
        return cell!
    }
    
  @objc func deleteRow(sender: UIButton){
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            let index = sender.tag
            let alertController = UIAlertController(title: "Message", message: "Do you want to delete this item?", preferredStyle: .alert)
            let actionCancel = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
            }
            let actionDelete = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction) in
                let url = URL(string: self.imageArr[index])
                let fileName =  (url)?.lastPathComponent
                let imageId = self.imageId[index]
                self.deleteImages(imageName: fileName!, imageId: imageId)
            }
            alertController.addAction(actionCancel)
            alertController.addAction(actionDelete)
            self.present(alertController, animated: true, completion: nil)
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            CommonClass.showAlert("No Internet Connection")
        }
    }
}
