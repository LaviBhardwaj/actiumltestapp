//
//  DateViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 27/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import Alamofire
import UserNotifications

class DateViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    // MARK: - IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    var zoneName = String()
    var retailerName = String()
    var shipName = String()
    var dateArray = [String]()
    var visitID = [String]()
    var visitRound = [String]()
    var dates = [Dates]()
    var networkStatus = Bool()
    var shipId = String()
    
    // MARK: - Views life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.dateUrl()
            self.networkStatus = true
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.networkStatus = false
            if self.shipId == UserDefaults.standard.string(forKey: "shipId") {
                self.dates = DataBaseHelper.shareInstance.getDateData()
            }
            else {
                let alertController = UIAlertController(title: "ActiumApp", message: "No Internet Connection", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        collectionView.register(UINib.init(nibName: "DateCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.networkStatus = true
            DataSyncManager.uploadSavedImages()
            DataSyncManager.uploadSavedVideo()
            DataSyncManager.uploadSavedReport()
            DataSyncManager.uploadSavedInventory()
            UNUserNotificationCenter.current().delegate = self 
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.networkStatus = false
        }
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    
    // MARK: - IBAction
    @IBAction func backBarBtn(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - CollectionView Datasource and Delegate

extension DateViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if networkStatus == true {
            return dateArray.count
        }
        else {
            return dates.count
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: DateCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DateCollectionViewCell
        if networkStatus == true {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            guard let date = dateFormatter.date(from: dateArray[indexPath.row]) else {
                fatalError()
            }
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy"
            let datestr = formatter.string(from: date)
            cell.dateLabel.text = datestr
            cell.visitRoundLbl.text = "Visit Round: \(visitRound[indexPath.row])"
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            guard let date = dateFormatter.date(from: dates[indexPath.row].dates!) else {
                fatalError()
            }
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy"
            let datestr = formatter.string(from: date)
            cell.dateLabel.text = datestr
            cell.visitRoundLbl.text = "Visit Round: \(dates[indexPath.row].visitRound!)"
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "ReportViewController") as? ReportViewController
        vc?.zoneName = self.zoneName
        vc?.retailerName = self.retailerName
        vc?.shipName = self.shipName
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            if self.dateArray.isEmpty {
                vc?.visitDate = self.dates[indexPath.row].dates!
                vc?.visitId = self.dates[indexPath.row].visitId!
                UserDefaults.standard.set(self.dates[indexPath.row].visitId, forKey: "VisitIdTemp")
            } else {
                vc?.visitDate = self.dateArray[indexPath.row]
                vc?.visitId = self.visitID[indexPath.row]
                UserDefaults.standard.set(self.visitID[indexPath.row], forKey: "VisitIdTemp")
            }
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            if self.dateArray.isEmpty {
                vc?.visitDate = self.dates[indexPath.row].dates!
                vc?.visitId = self.dates[indexPath.row].visitId!
            } else {
                vc?.visitDate = self.dateArray[indexPath.row]
                vc?.visitId = self.visitID[indexPath.row]
            }
        }
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    // MARK: - functions
    func dateUrl() {
        
        CommonClass.showLoader()
        let url = URL(string: "http://3.0.78.175/actium-dev/api/user/shipDateVisits")
        let params = ["port_name":zoneName,
                      "retailer_name":retailerName,
                      "user_id":UserDefaults.standard.string(forKey: "userid")!,
                      "brand_name":UserDefaults.standard.string(forKey: "brandName")!,
                      "type":UserDefaults.standard.string(forKey: "userTypeId")!,
                      "ship_name":shipName]
    
     Alamofire.request(url!, method: .post, parameters: params, encoding: URLEncoding.default, headers: CommonClass.apiKeyDict)
        .validate()
        .responseJSON(completionHandler: {  (data) in
            CommonClass.hideLoader()
            switch data.result {
            case .success(let dataa):
                print(dataa)
                DataBaseHelper.shareInstance.deleteDateData()
                if let dataresponse = dataa as? [String: Any]{
                    if let dataArray = dataresponse["data"] as? [[String: Any]] {
                        for i in 0 ..< dataArray.count {
                            let  date = dataArray[i]["training_date"] as? String
                            let  visitId = dataArray[i]["visit_id"] as? String
                            let visitRound = dataArray[i]["visit_round"] as? String
                            self.visitID.append(visitId!)
                            self.dateArray.append(date!)
                            self.visitRound.append(visitRound!)
                            let dict = ["date":date,"visitId":visitId,"VisitRound":visitRound]
                            DataBaseHelper.shareInstance.saveDate(object: dict as! [String : String])
                        }
                    }
                    self.collectionView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        })
    }
}
extension DateViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width / 2 - 5, height: UIScreen.main.bounds.size.width / 2 - 5)
    }
}







