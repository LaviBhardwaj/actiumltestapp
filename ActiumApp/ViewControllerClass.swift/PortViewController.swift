//
//  PortViewController.swift
//  ActiumApp
//
//  Created by Rahul Mishra on 12/12/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import AlamofireImage
import UserNotifications

class PortViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    var zoneName = String()
    var retailerName = String()
    var retailerId = String()
    var imageArray = [String]()
    var portName = [String]()
    var portId = [String]()
    var networkStatus = Bool()
    var port = [Port]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
                        self.portList()
                        self.networkStatus = true
                    }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
                        self.networkStatus = false
            if self.retailerId == UserDefaults.standard.string(forKey: "retailerId") {
                self.port = DataBaseHelper.shareInstance.getPortData()
            }
            else {
                let alertController = UIAlertController(title: "ActiumApp", message: "No Internet Connection", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        
        //MARK: - collection ViewCell Registration
        collectionView.register(UINib.init(nibName: "ZoneCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.networkStatus = true
            DataSyncManager.uploadSavedImages()
            DataSyncManager.uploadSavedVideo()
            DataSyncManager.uploadSavedReport()
            DataSyncManager.uploadSavedInventory()
            UNUserNotificationCenter.current().delegate = self
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.networkStatus = false
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    
    //MARK: - IBActions
    @IBAction func backBarBtn(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Apis
    func portList() {
        
        let params = ["zone_name":zoneName,
                      "retailer_name":retailerName,
                      "user_id":UserDefaults.standard.string(forKey: "userid")!,
                      "type":UserDefaults.standard.string(forKey: "userTypeId")!,
                      "brand_name":UserDefaults.standard.string(forKey: "brandName")!]
        
        CommonClass.getDataFromServer("portList", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") { result -> Void in
            print(result)
            if let dictResponse = result as? [String: Any] {
                if let status = dictResponse["status"] as? Bool,
                    status == true {
                    DataBaseHelper.shareInstance.deletePortData()
                    guard let data = dictResponse["data"] as? [[String: Any]] else {
                        return
                    }
                    for i in 0..<data.count {
                        guard let portName = data[i]["port_name"] as? String else {
                            return
                        }
                        guard let id = data[i]["id"] as? String else {
                            return
                        }
                        self.portName.append(portName)
                        self.portId.append(id)
                        let dict = ["portId":id,"portName":portName]
                        DataBaseHelper.shareInstance.savePort(object: dict)
                    }
                    self.collectionView.reloadData()
                }
                else {
                    let message = dictResponse["message"] as? String
                    let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
}
//MARK: - CollectionView Method
extension PortViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if networkStatus == true {
            return portName.count
        } else {
            return port.count
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ZoneCollectionViewCell
        if networkStatus == true {
            cell?.zoneNameLbl.text = portName[indexPath.row]
            let url = NSURL(string: UserDefaults.standard.string(forKey: "zoneImage")!)
            cell?.zoneImage.af_setImage(withURL: url! as URL)
            
        } else {
                cell?.zoneNameLbl.text = port[indexPath.row].portName!
            let url = NSURL(string: UserDefaults.standard.string(forKey: "zoneImage")!)
            cell?.zoneImage.af_setImage(withURL: url! as URL)
        }
                return cell!
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "ShipTableViewController") as? ShipTableViewController
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            vc?.retailerName = self.retailerName
            vc?.retailerId = self.retailerId
            vc?.zoneName = self.zoneName
            
            if  self.portName.isEmpty {
                vc?.portName = self.port[indexPath.row].portName!
                let portId = self.port[indexPath.row].portId!
                UserDefaults.standard.set(portId, forKey: "portId")
            } else {
                vc?.portName = self.portName[indexPath.row]
                let portId = self.portId[indexPath.row]
                UserDefaults.standard.set(portId, forKey: "portId")
            }
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            vc?.retailerName = self.retailerName
            vc?.retailerId = self.retailerId
            vc?.zoneName = self.zoneName
            if  self.portName.isEmpty {
                vc?.portName = self.port[indexPath.row].portName!
                vc?.portId = self.port[indexPath.row].portId!
            } else {
                vc?.portName = self.portName[indexPath.row]
                vc?.portId = self.portId[indexPath.row]
            }
        }
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
extension PortViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width / 2 - 5, height: UIScreen.main.bounds.size.width / 2 + 10)
    }
}
