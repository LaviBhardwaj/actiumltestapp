//
//  SalesViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 23/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import MMDrawController
import UserNotifications

class SalesViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    
    // MARK: - IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backbarBtn: UIButton!
    
    // MARK: - Properties
    var arrImg = [#imageLiteral(resourceName: "sales"),#imageLiteral(resourceName: "lrts.png")]
    
    // MARK: - Views life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        DataSyncManager.uploadSavedImages()

            backbarBtn.setBackgroundImage(#imageLiteral(resourceName: "left-arrow"), for: .normal)
        
          self.navigationController?.navigationBar.isHidden = true
        
        // MARK: - collection View Registration
        collectionView.register(UINib.init(nibName: "RetailerCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            DataSyncManager.uploadSavedImages()
            DataSyncManager.uploadSavedVideo()
            DataSyncManager.uploadSavedReport()
            DataSyncManager.uploadSavedInventory()
            UNUserNotificationCenter.current().delegate = self
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    
    // MARK: - IBActions
    
    @IBAction func backBarBtn(_ sender: UIBarButtonItem) {
            self.navigationController?.popViewController(animated: true)
            self.navigationController?.navigationBar.isHidden = false
    }
}
    // MARK: - Collection View datasource and delegate

extension SalesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell : RetailerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RetailerCell
        cell.salesImgs.image = arrImg[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if arrImg[indexPath.row] == arrImg[1] {
            
            let alertController = UIAlertController(title: "Continue!", message: "Do you want to see more by zone?", preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
               
            }
            let actionCancel = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    let story = UIStoryboard(name: "Main", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "ZoneViewController")
                    self.navigationController?.pushViewController(vc, animated: true)
            }
            
            alertController.addAction(actionCancel)
            alertController.addAction(actionOk)
            self.present(alertController, animated: true, completion: nil)
            
        }
        else {
            toastView(messsage: "Coming Soon", view: self.view)
        }
}
}
extension SalesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width / 2 + 30, height: UIScreen.main.bounds.size.width / 2 + 70)
    }
    
      func toastView(messsage : String, view: UIView ){
        let toastLabel = UILabel(frame: CGRect(x: view.frame.size.width/2 - 150, y: view.frame.size.height-100, width: 300,  height : 35))
        toastLabel.backgroundColor = UIColor.lightGray
        toastLabel.textColor = UIColor.black
        toastLabel.textAlignment = NSTextAlignment.center;
        view.addSubview(toastLabel)
        toastLabel.text = messsage
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        UIView.animate(withDuration: 4.0, delay: 0.1, options: UIViewAnimationOptions.curveEaseOut, animations: {
            toastLabel.alpha = 0.0
            
        })
    }
}
