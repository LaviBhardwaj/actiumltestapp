//
//  ReportViewController.swift
//  ActiumApp
//
//  Created by Rahul Mishra on 25/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import UserNotifications

class ReportViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    // MARK: - IBOutlet
    @IBOutlet var subView: [UIView]!
    
    // MARK: - Variables
    var visitDate = String()
    var visitId = String()
    var zoneName = String()
    var retailerName = String()
    var shipName = String()
    
    // MARK: - Views LIfe cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        UserDefaults.standard.set("", forKey: "isApiCall")
        for item in subView{
            item.layer.borderWidth = 2
            item.layer.borderColor = UIColor.white.cgColor
            item.layer.cornerRadius = 20
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
}
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            DataSyncManager.uploadSavedImages()
            DataSyncManager.uploadSavedVideo()
            DataSyncManager.uploadSavedReport()
            DataSyncManager.uploadSavedInventory()
            UNUserNotificationCenter.current().delegate = self
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    
    
    // MARK: - IBActions
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func imageActnBtn(_ sender: UIButton) {
        
            let story = UIStoryboard(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "CapturePicViewController") as? CapturePicViewController
            vc?.visitDate = self.visitDate
            vc?.visitId = self.visitId
            vc?.zoneName = self.zoneName
            vc?.retailerName = self.retailerName
            vc?.shipName = self.shipName
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    
    @IBAction func videoActnBtn(_ sender: UIButton) {
        
            let story = UIStoryboard(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "VideoListViewController") as? VideoListViewController
            vc?.visitID = self.visitId
            vc?.visitDAte = self.visitDate
            vc?.zoneName = self.zoneName
            vc?.retailerName = self.retailerName
            vc?.shipName = self.shipName
            self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func reportBtnActn(_ sender: UIButton) {
        
        let story = UIStoryboard(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "ReportListViewController") as? ReportListViewController
            vc?.visitDAte = self.visitDate
            vc?.visitID = self.visitId
            vc?.zoneName = self.zoneName
            vc?.retailerName = self.retailerName
            vc?.shipName = self.shipName
            self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func inventoryBtnActn(_ sender: UIButton) {
        
            let story = UIStoryboard(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "InventoryViewController") as? InventoryViewController
            vc?.visitDAte = self.visitDate
            vc?.visitID = self.visitId
            vc?.zoneName = self.zoneName
            vc?.retailerName = self.retailerName
            vc?.shipName = self.shipName
            self.navigationController?.pushViewController(vc!, animated: true)
    }
}



