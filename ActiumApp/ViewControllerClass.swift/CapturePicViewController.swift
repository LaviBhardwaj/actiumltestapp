
//
//  CapturePicViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 26/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import OpalImagePicker
import Photos
import Foundation
import AVFoundation
import Alamofire
import AlamofireImage
import UserNotifications
    
class CapturePicViewController: UIViewController, UIImagePickerControllerDelegate, OpalImagePickerControllerDelegate, UINavigationControllerDelegate, UNUserNotificationCenterDelegate {
    
    // MARK: - IBOutlet
    @IBOutlet weak var uploadImageBtn: UIButton!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addImageBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var deleteImagebtn: UIButton!
    
    
    // MARK: - variables
    var data = [Any]()
    var selectedImage = [UIImage]()
    var isDidFinishPickingAssetsCalled = false
    var isDidFinishPickingExternalCalled = false
    var isCancelledCalled = false
    var visitId = String()
    var visitDate = String()
    var zoneName = String()
    var retailerName = String()
    var shipName = String()
    var imagesArray = [String]()
    var imageStatus = Bool()
    var imageStr = [String]()
    var selectImages: (([UIImage]) -> Void)?
    var selectURLs: (([URL]) -> Void)?
    var externalURLForIndex: ((Int) -> URL?)?
    var numberOfExternalItems = 0
    var tempImagArray = [String]()
    var imageString = String()
    var externalItemsTitle = NSLocalizedString("External", comment: "External (Segmented Control Title)")
    var multipleImage = [MultipleImages]()
    var uploadingImage = [UploadingImages]()
    var netWorkStatus = Bool()
    var content = UNMutableNotificationContent()
    
    // MARK: - Views Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 3.0
        scrollView.zoomScale = 1.0
        
        if UserDefaults.standard.string(forKey: "userType") == "User" {
            addImageBtn.isHidden = false
        }
        else{
            addImageBtn.isHidden = true
            deleteImagebtn.isHidden = true
        }
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.netWorkStatus = true
            self.multipleImages()
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.netWorkStatus = false
         if self.visitId == UserDefaults.standard.string(forKey: "VisitIdTemp") {
            self.multipleImage = DataBaseHelper.shareInstance.getMultipleImages()
            //self.mainImage.af_setImage(withURL: NSURL(string: self.multipleImage[0].imageStr!)! as URL)
         }
            else {
                let alertController = UIAlertController(title: "ActiumApp", message: "No Internet Connection", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
            }
    
        imageStatus = false
        let nib = UINib(nibName: "RetailerCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "cell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        uploadImageBtn.isHidden = true
        uploadImageBtn.layer.borderWidth = 2
        uploadImageBtn.layer.borderColor = UIColor.black.cgColor
        uploadImageBtn.layer.cornerRadius = uploadImageBtn.frame.size.height/2
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return mainImage
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            if let isApicall = UserDefaults.standard.string(forKey: "isApiCall") {
                if isApicall.count > 1 {
                    self.multipleImages()
                }
            }
            DataSyncManager.uploadSavedImages()
            DataSyncManager.uploadSavedVideo()
            DataSyncManager.uploadSavedReport()
            DataSyncManager.uploadSavedInventory()
            UNUserNotificationCenter.current().delegate = self
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.netWorkStatus = false
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func backBarBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteImgBtnActn(_ sender: UIButton) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "DeletePhotosController") as? DeletePhotosController
        vc?.visitId = self.visitId
        vc?.visitDate = self.visitDate
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func uploadImageBtn(_ sender: UIButton) {
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.uploadImages()
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            let dict = ["UploadingImages":self.imageString,"VisitId":self.visitId,"Visitdate":self.visitDate,"ZoneName":self.zoneName,"ShipName":self.shipName,"RetailerName":self.retailerName]
            DataBaseHelper.shareInstance.saveUploadingImage(object: dict)
            self.imageString.removeAll()
            CommonClass.showAlert("No Internet Connection Data will Sync When Internet Appeared")
            self.uploadImageBtn.isHidden = true
        }
        
    }
    @IBAction func chooseImgBtn(_ sender: Any) {
        
        imageStatus = true
        let  alertController = UIAlertController(title: nil, message: nil,preferredStyle: .actionSheet)
        
        let action1 = UIAlertAction(title: "Photo Gallery", style: .default) { (action:UIAlertAction) in
            
            let imagePicker = OpalImagePickerController()
            imagePicker.imagePickerDelegate = self
            guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
                //Show error to user?
                return
            }
          self.present(imagePicker, animated: true, completion: nil)
          
        }
        let action2 = UIAlertAction(title: "Camera", style: .default) { (action:UIAlertAction) in
            
            let  imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        let action3 = UIAlertAction(title: "cancel", style: .cancel) { (action:UIAlertAction) in
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(action3)
        self.present(alertController, animated: true, completion: nil)
    }
    
  // MARK: - OpalImagePickerControllerDelegate
  func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        //Cancel action?
    }
    
   func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        self.uploadImageBtn.isHidden = false
        //Save Images, update UI
         selectedImage = images
        mainImage.image = selectedImage[0]
        
        for (_, item) in selectedImage.enumerated() {
            let compressingValue:CGFloat = 0.2
            let imageData = UIImageJPEGRepresentation(item, compressingValue)!
            let dataBase64Str = imageData.base64EncodedString(options: [])
            tempImagArray.append(dataBase64Str)
            imageString = tempImagArray.joined(separator: ",")
        }
        
        self.collectionView.reloadData()
        //Dismiss Controller
        presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerNumberOfExternalItems(_ picker: OpalImagePickerController) -> Int {
        return 1
    }
    
    func imagePickerTitleForExternalItems(_ picker: OpalImagePickerController) -> String {
        return NSLocalizedString("External", comment: "External (title for UISegmentedControl)")
    }
    
    func imagePicker(_ picker: OpalImagePickerController, imageURLforExternalItemAtIndex index: Int) -> URL? {
        return URL(string: "https://placeimg.com/500/500/nature")
    }
}

// MARK: - collectionView Delegate and Datasource
extension CapturePicViewController: UICollectionViewDelegate, UICollectionViewDataSource  {
    
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if netWorkStatus == true {
        if imageStatus == false {
               return imagesArray.count
        }
            else {
            return selectedImage.count
        }
    } else {
        if imageStatus == false {
               return multipleImage.count
        }
            else {
            return selectedImage.count
        }
    }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: RetailerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RetailerCell
     if netWorkStatus == true {
            if imageStatus == false {
                if let url = NSURL(string: imagesArray[indexPath.row]) {
                    cell.salesImgs.af_setImage(withURL: url as URL)
        }
        }  else {
                    cell.salesImgs.image = selectedImage[indexPath.row]
        }
        }
     else {
            if imageStatus == false {
                if self.multipleImage.count != 0 {
                    self.mainImage.af_setImage(withURL: NSURL(string: self.multipleImage[0].imageStr!)! as URL)
                    if let url = NSURL(string: multipleImage[indexPath.row].imageStr!) {
                        cell.salesImgs.af_setImage(withURL: url as URL)
                }
        }
        }  else {
                    cell.salesImgs.image = selectedImage[indexPath.row]
        }
        }
        return cell
     }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
      if netWorkStatus == true {
        
             if imageStatus == false {
                  if let url = NSURL(string: imagesArray[indexPath.row]) {
                    mainImage.af_setImage(withURL: url as URL)
                  }
             
             } else {
                    mainImage.image = selectedImage[indexPath.row]
       }
       } else {
             if imageStatus == false {
                  if let url = NSURL(string: multipleImage[indexPath.row].imageStr!) {
                    mainImage.af_setImage(withURL: url as URL)
                  }
             }
             else {
                    mainImage.image = selectedImage[indexPath.row]
            }
        
         }
    }
    
// MARK: - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.uploadImageBtn.isHidden = false
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            tempImagArray.removeAll()
            mainImage.image = image
            selectedImage.append(image)
            let compressingValue:CGFloat = 0.2
            let data:Data = UIImageJPEGRepresentation(image, compressingValue)!
            let dataBase64Str = data.base64EncodedString(options: [])
            tempImagArray.append(dataBase64Str)
            imageString = tempImagArray.joined(separator: ",")
            self.collectionView.reloadData()
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        } else
        {
            print("Something went wrong")
        }
        picker.dismiss(animated: true, completion: nil)
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }

    
    
    
    // MARK: - Apis
    func multipleImages() {
        
        let params = ["visit_id":visitId,
                      "visit_date":visitDate]
        CommonClass.showLoader()
        CommonClass.getDataFromServer("multipleImageList", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") { result -> Void in
            CommonClass.hideLoader()
            if let dictResponse = result as? [String: Any] {
                if let status = dictResponse["status"] as? Bool,
                    status == true {
                    self.imageStatus = false
                    self.netWorkStatus = true
                    UserDefaults.standard.set("", forKey: "isApiCall")
                    self.imagesArray.removeAll()
                    DataBaseHelper.shareInstance.deleteMultipleImages()
                    guard let data = dictResponse["data"] as? [[String: Any]] else {
                        return
                    }
                    for i in 0 ..< data.count{
                        if let imagePath = data[i]["image_path"] as? String {
                                self.imagesArray.append(imagePath)
                            self.mainImage.af_setImage(withURL: NSURL(string: self.imagesArray[0])! as URL)
                                let dict = ["Images":imagePath]
                                DataBaseHelper.shareInstance.saveMultipleImages(object: dict)
                        }
                    }
                    self.collectionView.reloadData()
                  }
                else {
                    self.imagesArray.removeAll()
                    self.mainImage.image = nil
                    self.collectionView.reloadData()
                    UserDefaults.standard.set("", forKey: "isApiCall")
                    let message = dictResponse["message"] as? String
                    let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                        print("You've pressed cancel")
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
           }
      }
}
    
    func uploadImages() {
        
        let params = ["visit_id":visitId,
                      "zone_name":zoneName,
                      "retailer_name":retailerName,
                      "ship_name":shipName,
                      "visit_date":visitDate,
                      "img_name": imageString
                        ] as [String : Any]
        CommonClass.showLoader()
        CommonClass.getDataFromServer("uploadMultipleImages", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") { result -> Void in
            CommonClass.hideLoader()
            if let dictResponse = result as? [String: Any] {
                if let status = dictResponse["status"] as? Bool,
                    status == true {
                    DataBaseHelper.shareInstance.deleteUploadingImages()
                    self.uploadImageBtn.isHidden = true
                    let message = dictResponse["message"] as? String
                    
                    self.content.title = "Message"
                    self.content.body = message!
                    self.content.badge = 1
                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
                    
                    //getting the notification request
                    let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: self.content, trigger: trigger)
                    
                    UNUserNotificationCenter.current().delegate = self
                    
                    //adding the notification to notification center
                    UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                    
                    let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                        self.multipleImages()
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
                else {
                    let message = dictResponse["message"] as? String
                    let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
}



