//
//  LoginViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 22/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import Alamofire

class LoginViewController: UIViewController {
    
    // MARK: - IBOutlets
    
//    @IBOutlet var roundedView: [UIView]!
    @IBOutlet weak var userTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet var signInBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var txtUserType: UITextField!
    
    
    // MARK: - Properties
    var status : Bool?
    var picker = UIPickerView()
    var userTypeArr = ["Actium User","Brand","Retailer"]
    var userTypeIdArr = ["2","3","4"]
    var id = String()
    
    // MARK: - View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker.delegate = self
        picker.dataSource = self
        txtUserType.inputView = picker
        
        
        
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        signInBtn.layer.cornerRadius = signInBtn.frame.size.height/2
        signInBtn.layer.borderWidth = 1
        signInBtn.layer.borderColor = UIColor.lightGray.cgColor
        signUpBtn.layer.cornerRadius = signInBtn.frame.size.height/2
        
    // MARK: - placeholder Attributes
        userTF.attributedPlaceholder =
            NSAttributedString(string: "User Name",                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        passwordTF.attributedPlaceholder =
            NSAttributedString(string: "Password",                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        txtUserType.attributedPlaceholder =
            NSAttributedString(string: "Select User Type",                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    
@IBAction func signUpBtn(_ sender: UIButton) {
        
          let story = UIStoryboard(name: "Main", bundle: nil)
          let vc = story.instantiateViewController(withIdentifier:"RagistrationViewController" )
          self.navigationController?.pushViewController(vc, animated: true)
    }
    
@IBAction func signInBtn(_ sender: Any) {
        
    if userTF.text?.count == 0 {
        
        let alertController = UIAlertController(title: "Alert!", message: "Please enter User Name", preferredStyle: .alert)
            
        let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                print("You've pressed cancel")
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            }
        
    else if passwordTF.text?.count == 0 {
        
        let alertController = UIAlertController(title: "Alert!", message: "Please enter password", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
            print("You've pressed cancel")
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            }
    else if txtUserType.text?.count == 0 {
        CommonClass.showAlert("Please Select User Type")
    }
        
        else {
        print(txtUserType.text!)
             CommonClass.NetworkManager.isReachable { networkManagerInstance in
             self.hitMethod()
             }
             CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
              CommonClass.showAlert("No Internet Connection")
            }
        }
}
    
   // MARK: - Functions
    
func hitMethod() {
    
    CommonClass.showLoader()
    
    let param : [String: Any] = ["email": userTF.text as Any,
                     "password":passwordTF.text as Any,
                     "type": self.id as Any,
                     ]
        let url = URL(string: "http://3.0.78.175/actium-dev/api/user/login")
        let apiKeyDict = ["X-API-KEY":"86451504487681cb3012f4132da78e64"]
    print(param)
    Alamofire.request(url!, method: .post, parameters: param as Any as? Parameters,encoding: JSONEncoding.default, headers: apiKeyDict)
        .validate()
        .responseData(completionHandler: { (data) in
                CommonClass.hideLoader()
    switch data.result {
        case .success(let dataa):
              if let dictResponse = self.convertToDictionary(dataaa: dataa) {
                print(dictResponse)
                         if let status = dictResponse["status"] as? Bool,
                              status == true {
                            
                                  guard let userid = dictResponse["userid"] as? String else {
                                  return
                            }
                            UserDefaults.standard.set(userid, forKey: "userid")
                            
                            if let retailerName = dictResponse["user_type_name"] as? String {
                            UserDefaults.standard.set(retailerName, forKey: "retailer_name")
                            }
                            guard let userType = dictResponse["user_type"] as? String else {
                                return
                            }
                            UserDefaults.standard.set(userType, forKey: "userType")
                                self.gotoHome()
                            }
                else {
                            let alertController = UIAlertController(title: "Message", message: dictResponse["message"] as? String, preferredStyle: .alert)
                            
                            let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                                print("You've pressed cancel")
                            }
                            alertController.addAction(action)
                            
                            self.present(alertController, animated: true, completion: nil)
                            print(dictResponse["message"] as? String ?? "")
                    }
              }
                else{
                            print("Response is not successfully converted")
                    }
         case .failure(let error):
                            print(error)
                }
         })
    }
    
    // MARK: - Functions
    func convertToDictionary(dataaa: Data) -> [String: Any]? {
        do {
            return try JSONSerialization.jsonObject(with: dataaa, options: []) as? [String: Any]
    }   catch {
            print(error.localizedDescription)
        }
            return nil
    }
    
    func gotoHome() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc: ViewController  = story.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - textfield delegate

extension LoginViewController : UITextFieldDelegate {
    
       func textFieldDidEndEditing(_ textField: UITextField) {
        
       }
       func textFieldShouldReturn(_ textField: UITextField) -> Bool {
             return self.view.endEditing(true)
       }
}

class RoundedView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 15
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
}

extension LoginViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return userTypeArr.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return userTypeArr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        txtUserType.text = userTypeArr[row]
        self.id = userTypeIdArr[row]
        UserDefaults.standard.set(userTypeIdArr[row], forKey: "userTypeId")
        print(UserDefaults.standard.string(forKey: "userTypeId")!)
    }
    
}
