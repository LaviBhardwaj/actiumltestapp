//
//  ZoneViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 23/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import CoreData
import UserNotifications

class ZoneViewController: UIViewController , UNUserNotificationCenterDelegate {
    
    // MARK: - IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    var arrName = [String]()
    var zoneId = [String]()
    var savedZoneId = String()
    var zoneIdTemp = String()
    var networkStatus = Bool()
    var zoneImage = [UIImage]()
    var zone = [Zone]()
    var arrImg = [String]()
    
    
    // MARK: - Views life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.zoneList()
            self.networkStatus = true
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.networkStatus = false
            print(UserDefaults.standard.string(forKey: "brandIdSaved")!)
            print(UserDefaults.standard.string(forKey: "brandIdTemp")!)
            if UserDefaults.standard.string(forKey: "brandIdSaved") == UserDefaults.standard.string(forKey: "brandIdTemp") {
                self.zone = DataBaseHelper.shareInstance.getZoneData()
            }
            else {
                let alertController = UIAlertController(title: "ActiumApp", message: "No Internet Connection", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
            }
            
//            if self.zone.count == 0 {
            
//        }
        
     // MARK: - collection View Registration
        
        collectionView.register(UINib.init(nibName: "ZoneCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
}
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            //self.networkStatus = true
            DataSyncManager.uploadSavedImages()
            DataSyncManager.uploadSavedVideo()
            DataSyncManager.uploadSavedReport()
            DataSyncManager.uploadSavedInventory()
            UNUserNotificationCenter.current().delegate = self
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            //self.networkStatus = false
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    
    // MARK: - IBAction
    @IBAction func backToSalesBtn(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}

    // MARK: - Collection View datasource and delegate
extension ZoneViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if networkStatus == true {
            return zoneId.count
        } else {
            return zone.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : ZoneCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ZoneCollectionViewCell
        
//        cell.zoneNameLbl.text = arrName[indexPath.row]
//        cell.retailImg.image = zoneImage[indexPath.row]
        
        
        if networkStatus == true {
            let url = NSURL(string: arrImg[indexPath.row])
            cell.zoneImage.af_setImage(withURL: url! as URL)
            cell.zoneNameLbl.text = arrName[indexPath.row]
        } else {
            if let url = NSURL(string: zone[indexPath.row].image!) {
                cell.zoneImage.af_setImage(withURL: url as URL)
                cell.zoneNameLbl.text = zone[indexPath.row].name
            }
        }
            CommonClass.hideLoader()
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        if UserDefaults.standard.string(forKey: "userType") == "Retailer"{
//
//            CommonClass.NetworkManager.isReachable { networkManagerInstance in
//                let story = UIStoryboard(name: "Main", bundle: nil)
//                let vc:ShipTableViewController  = story.instantiateViewController(withIdentifier: "ShipTableViewController") as! ShipTableViewController
//                vc.zoneName = self.arrName[indexPath.row]
//                vc.retailerName = UserDefaults.standard.string(forKey: "retailer_name")!
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//            CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
//                CommonClass.showAlert("No Internet Connection")
//            }
//        }
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc:RetailerViewController  = story.instantiateViewController(withIdentifier: "RetailerViewController") as! RetailerViewController
        
            if networkStatus == true  {
                vc.zoneName = self.arrName[indexPath.row]
                let zoneImage = arrImg[indexPath.row]
                UserDefaults.standard.set(zoneImage, forKey: "zoneImage")
                zoneIdTemp = zoneId[indexPath.row]
                UserDefaults.standard.set(zoneIdTemp, forKey: "zoneIdTemp")
            }
            else {
                let zoneImage = zone[indexPath.row].image
                UserDefaults.standard.set(zoneImage, forKey: "zoneImage")
                vc.zoneName = zone[indexPath.row].name!
                vc.zoneId = zone[indexPath.row].id!
            }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - APis
    func zoneList() {

        CommonClass.showLoader()
        guard let userId = UserDefaults.standard.string(forKey: "userid") else {
            return
        }
        guard let userType = UserDefaults.standard.string(forKey: "userTypeId") else {
            return
        }
        
        
        //http://3.0.78.175/actium-dev/api/user/zoneList?user_id=2
        
        
        let url = "http://3.0.78.175/actium-dev/api/user/zoneList?user_id=\(userId)&user_type=\(userType)"
        print(url)
        Alamofire.request(url, method: .get, parameters: nil ,encoding: JSONEncoding.default, headers: CommonClass.apiKeyDict)
            .validate()
            .responseJSON(completionHandler: { (data) in
                print(data)
                CommonClass.hideLoader()
                switch data.result {
                case .success(let data):
            if  let dictResponse = data as? [String : Any] {
                 if let status = dictResponse["status"] as? Bool,
                    status == true {
                       DataBaseHelper.shareInstance.deleteZoneData()
                         if let dataArray = dictResponse["data"] as? [[String: Any]] {
                            for i in 0 ..< dataArray.count{
                                
                                if let nameArray = dataArray[i]["zone_name"] as? String {
                                    self.arrName.append(nameArray)
                                }else{
                                    self.arrName.append("")
                                }
                                
                                if let Id = dataArray[i]["id"] as? String {
                                    self.zoneId.append(Id)
                                }
                                
                            if let imgStr = dataArray[i]["zone_image"] as? String{
                        let imgStr1 = imgStr.replacingOccurrences(of: " ", with: "%20")
                        self.arrImg.append(imgStr1)
                                let dict = ["imageString":imgStr1,"zoneName":self.arrName[i],"zoneId":self.zoneId[i]]
                                DataBaseHelper.shareInstance.saveZone(object: dict)
                    }
                }
                    self.collectionView.reloadData()
                        }
                    }
                 else {
                    let message = dictResponse["message"] as? String
                    let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    CommonClass.showAlert("Something went wrong, Please try again")
                }
            })
    }
    
}
extension ZoneViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width / 2 - 5, height: UIScreen.main.bounds.size.width / 2 + 10)
    }
}



