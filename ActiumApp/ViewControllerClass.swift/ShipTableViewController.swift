//
//  ShipTableViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 27/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import Alamofire
import UserNotifications

class ShipTableViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    // MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    var zoneName = String()
    var portName = String()
    var retailerName = String()
    var retailerId = String()
    var shipNamelist = [String]()
    var shipName = String()
    var networkStatus = Bool()
    var ship = [Ship]()
    var portId = String()
    var shipId = [String]()
    
    // MARK: - Views Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.shipList()
            self.networkStatus = true
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.networkStatus = false
            if self.portId == UserDefaults.standard.string(forKey: "portId") {
                self.ship = DataBaseHelper.shareInstance.getShipData()
            }
            else {
                let alertController = UIAlertController(title: "ActiumApp", message: "No Internet Connection", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        
        let nib = UINib(nibName: "ShipTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "cell")
        self.tableView.separatorStyle = .none
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.networkStatus = true
            DataSyncManager.uploadSavedImages()
            DataSyncManager.uploadSavedVideo()
            DataSyncManager.uploadSavedReport()
            DataSyncManager.uploadSavedInventory()
            UNUserNotificationCenter.current().delegate = self
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.networkStatus = false
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    
    // MARK: - IBActions
    @IBAction func backBarBtn(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - TableView Datasource and Delegate

extension ShipTableViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if networkStatus == true {
            return shipNamelist.count
        }
        else {
            return ship.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ShipTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ShipTableViewCell
        if networkStatus == true {
            cell.shipNameLbl.text = shipNamelist[indexPath.row]
        } else {
            cell.shipNameLbl.text = ship[indexPath.row].shipName
        }
        cell.tableBtn.tag = indexPath.row
        cell.tableBtn.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc: DateViewController = story.instantiateViewController(withIdentifier: "DateViewController") as! DateViewController
        vc.zoneName = self.portName
        vc.retailerName = self.retailerName
        
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            if self.shipNamelist.isEmpty {
                vc.shipName = self.ship[indexPath.row].shipName!
                let shipIdTemp = self.ship[indexPath.row].id!
                UserDefaults.standard.set(shipIdTemp, forKey: "shipId")
            } else {
                vc.shipName = self.shipNamelist[indexPath.row]
                let shipIdTemp = self.shipId[indexPath.row]
                UserDefaults.standard.set(shipIdTemp, forKey: "shipId")
            }
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            if self.shipNamelist.isEmpty {
                vc.shipName = self.ship[indexPath.row].shipName!
                vc.shipId = self.ship[indexPath.row].id!
            } else {
                vc.shipName = self.shipNamelist[indexPath.row]
                vc.shipId = self.shipId[indexPath.row]
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func buttonPressed(_ sender: AnyObject) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "ytgYtdViewController") as? ytgYtdViewController
        let point: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        let index = tableView.indexPathForRow(at: point)
        
        if shipNamelist.count == 0 {
            vc?.shipName = ship[0].shipName!
        } else {
            vc?.shipName = shipNamelist[(index?.row)!]
        }
        vc?.zoneId = zoneName
        vc?.brandId = retailerId
        vc?.retailerName = retailerName
        vc?.portName = portName
        
        self.present(vc! , animated: true, completion : nil)
    }
    
    // MARK: - Functions
    
    func shipList() {
        
        CommonClass.showLoader()
        let url = URL(string: "http://3.0.78.175/actium-dev/api/user/shipList")
        let params = ["port_name":portName,
                      "zone_name": self.zoneName,
                      "retailer_name":retailerName,
                      "user_id":UserDefaults.standard.string(forKey: "userid")!,
                      "type":UserDefaults.standard.string(forKey: "userTypeId")!,
                      "brand_name":UserDefaults.standard.string(forKey: "brandName")!]
        print(params)
        Alamofire.request(url!, method: .post, parameters: params, encoding: URLEncoding.default, headers: CommonClass.apiKeyDict)
            .validate()
            .responseJSON(completionHandler: { (data) in
                CommonClass.hideLoader()
                switch data.result {
                case .success(let dataa):
                    print(dataa)
                    DataBaseHelper.shareInstance.deleteShipData()
                    if let dictResponse = dataa as? [String: Any] {
                        if let status = dictResponse["status"] as? Bool,
                            status == true {
                        if let dataArray = dictResponse["data"] as? [[String: Any]] {
                            for i in 0 ..< dataArray.count {
                                guard let nameShip = dataArray[i]["ship"] as? String else {
                                    return
                                }
                                guard let id = dataArray[i]["id"] as? String else {
                                    return
                                }
                                self.shipId.append(id)
                                self.shipNamelist.append(nameShip)
                                let dict = ["shipName":nameShip,"id":id,]
                                DataBaseHelper.shareInstance.saveShip(object: dict)
                            }
                        }
                        }
                        else {
                            let message = dictResponse["message"] as? String
                            let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                            let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                                self.navigationController?.popViewController(animated: true)
                            }
                            alertController.addAction(action)
                            self.present(alertController, animated: true, completion: nil)
                        }
                        self.tableView.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
            })
    }
}




