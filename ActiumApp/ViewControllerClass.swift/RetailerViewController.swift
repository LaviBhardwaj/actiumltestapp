//
//  RetailerViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 22/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import UserNotifications

class RetailerViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    // MARK: - IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    var arryImg = [String]()
    var zoneName = String()
    var retailerName = [String]()
    var retailerId = [String]()
    var retailer = [Retailer]()
    var networkStatus = Bool()
    var zoneId = String()
    var zoneIdWithNet = String()
    
    // MARK: - Views life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.retilerList()
            self.networkStatus = true
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.networkStatus = false
            if self.zoneId == UserDefaults.standard.string(forKey: "zoneIdTemp") {
                self.retailer = DataBaseHelper.shareInstance.getRetailerData()
            }
            else {
                let alertController = UIAlertController(title: "ActiumApp", message: "No Internet Connection", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
   // MARK: - collection ViewCell Registration
        let nib = UINib(nibName: "RetailerCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "cell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.networkStatus = true
            DataSyncManager.uploadSavedImages()
            DataSyncManager.uploadSavedVideo()
            DataSyncManager.uploadSavedReport()
            DataSyncManager.uploadSavedInventory()
            UNUserNotificationCenter.current().delegate = self
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.networkStatus = false
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
   
    // MARK: - IBAtions
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
}

    // MARK: - CollectionView Datasource and Delegate

extension RetailerViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if networkStatus == true {
            return arryImg.count
        }
        else {
            return retailer.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: RetailerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RetailerCell
        if networkStatus == true {
            if let url = NSURL(string: arryImg[indexPath.row]) {
                cell.retailImg?.af_setImage(withURL: url as URL, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (image) in
                    
                    let imageWithChangedColor = cell.retailImg.image?.withRenderingMode(.alwaysTemplate)
                    cell.retailImg.tintColor = .white
                    cell.retailImg.image = imageWithChangedColor
                }
            }
        } else {
            if let url = NSURL(string: retailer[indexPath.row].imageStr!) {
                cell.retailImg?.af_setImage(withURL: url as URL, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (image) in
                    
                    let imageWithChangedColor = cell.retailImg.image?.withRenderingMode(.alwaysTemplate)
                    cell.retailImg.tintColor = .white
                    cell.retailImg.image = imageWithChangedColor
                }
            }
        }
        CommonClass.hideLoader()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc  = story.instantiateViewController(withIdentifier: "PortViewController") as! PortViewController
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            vc.zoneName = self.zoneName
            if  self.retailerName.isEmpty {
                vc.retailerName = self.retailer[indexPath.row].name!
                let retailerIdTemp = self.retailer[indexPath.row].id!
                vc.retailerId = self.retailer[indexPath.row].id!
                UserDefaults.standard.set(retailerIdTemp, forKey: "retailerId")
            } else {
                vc.retailerName = self.retailerName[indexPath.row]
                let retailerIdTemp = self.retailerId[indexPath.row]
                vc.retailerId = self.retailerId[indexPath.row]
                UserDefaults.standard.set(retailerIdTemp, forKey: "retailerId")
            }
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            vc.zoneName = self.zoneName
            if  self.retailerName.isEmpty {
                vc.retailerName = self.retailer[indexPath.row].name!
                vc.retailerId = self.retailer[indexPath.row].id!
            } else {
                vc.retailerName = self.retailerName[indexPath.row]
                vc.retailerId = self.retailerId[indexPath.row]
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - APis
    func retilerList() {
        
        CommonClass.showLoader()
        let url = URL(string: "http://3.0.78.175/actium-dev/api/user/retailerList")
        let params: [String:Any] = ["zone_name":zoneName,
                                    "user_id":UserDefaults.standard.string(forKey: "userid")!,
                                    "type":UserDefaults.standard.string(forKey: "userTypeId")!,
                                    "brand_name":UserDefaults.standard.string(forKey: "brandName")!]
        
    Alamofire.request(url!, method: .post, parameters: params, encoding: URLEncoding.default, headers: CommonClass.apiKeyDict)
        .validate()
        .responseJSON(completionHandler: { (data) in
            print(data)
            CommonClass.hideLoader()
            switch data.result {
            case .success(let dataa):
                if  let dictResponse = dataa as? [String: Any] {
                    if let status = dictResponse["status"] as? Bool,
                        status == true {
                        DataBaseHelper.shareInstance.deleteRetailerData()
                    if let dataArray = dictResponse["data"] as? [[String: Any]] {
                        for i in 0 ..< dataArray.count{
                        let imageArray = dataArray[i]["retailer_logo"] as? String
                        let nameArray = dataArray[i]["retailer_name"] as? String
                            guard let retailerId = dataArray[i]["id"] as? String else {
                                return
                            }
                            self.retailerName.append(nameArray!)
                            self.arryImg.append(imageArray!)
                            self.retailerId.append(retailerId)
                            let dict = ["imageString":imageArray,"id":retailerId,"retailerName":nameArray]
                            DataBaseHelper.shareInstance.saveRetailer(object: dict as! [String : String])
                        }
                        }
                    }
                    else{
                        let message = dictResponse["message"] as? String
                        let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                        
                        let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                            self.navigationController?.popViewController(animated: true)
                            print("You've pressed cancel")
                        }
                        alertController.addAction(action)
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                    self.collectionView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        })
    }
}
extension RetailerViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width / 2 - 5, height: UIScreen.main.bounds.size.width / 2 - 5)
    }
}







