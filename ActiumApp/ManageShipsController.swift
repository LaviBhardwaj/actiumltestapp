//
//  ManageSCViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 22/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit

class ManageShipsController: UIViewController,UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var tablView: UITableView!
    
    // MARK: - Views Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tablView.delegate = self
        tablView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - textfield delegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return self.view.endEditing(true)
    }


    // MARK: - TableView Datasource and Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ManageShipCell")
        return cell!
    }

}

