//
//  Profile+CoreDataProperties.swift
//  
//
//  Created by Techsaga Corporations on 05/12/18.
//
//

import Foundation
import CoreData

extension Profile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profile> {
        return NSFetchRequest<Profile>(entityName: "Profile")
    }
    @NSManaged public var mobile: String?
    @NSManaged public var brandName: String?
    @NSManaged public var firstName: String?
    @NSManaged public var email: String?
    @NSManaged public var image: String?
    @NSManaged public var lastName: String?
}

extension Brand {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Brand> {
        return NSFetchRequest<Brand>(entityName: "Brand")
    }
    @NSManaged public var ImageStr: String?
}

