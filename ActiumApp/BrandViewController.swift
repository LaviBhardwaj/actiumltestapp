//
//  BrandViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 23/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit

class BrandViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    // MARK: - Views life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
    // MARK: - collection View Registration
        collectionView.register(UINib.init(nibName: "RetailerCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

  // MARK: - Collection View datasource and delegate
extension BrandViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        return cell
    }
}
