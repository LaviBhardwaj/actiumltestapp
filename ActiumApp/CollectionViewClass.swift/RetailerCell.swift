//
//  RetailerCell.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 22/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit

class RetailerCell: UICollectionViewCell {
    
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var retailImg: UIImageView!
    
    @IBOutlet weak var salesImgs: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.zoneImage.layer.cornerRadius = self.zoneImage.frame.size.width/2
//        self.zoneImage.layer.masksToBounds = false
//        self.zoneImage.contentMode = UIViewContentMode.scaleAspectFit

        self.retailImg.layer.masksToBounds = true
        roundView.layer.borderWidth = 2
        roundView.layer.borderColor = UIColor.white.cgColor
        roundView.layer.cornerRadius = 15
        
    }

}
