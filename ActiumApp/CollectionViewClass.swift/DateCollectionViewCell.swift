//
//  DateCollectionViewCell.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 27/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit

class DateCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var visitRoundLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.roundView.layer.borderWidth = 2
        self.roundView.layer.borderColor = UIColor.white.cgColor
        self.roundView.layer.cornerRadius = 15
        
    }

}
