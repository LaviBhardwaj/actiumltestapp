//
//  ShipTableViewCell.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 27/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit

class ShipTableViewCell: UITableViewCell {

    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var shipNameLbl: UILabel!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var tableBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.roundView.layer.borderWidth = 2
        self.roundView.layer.borderColor = UIColor.white.cgColor
        self.roundView.layer.cornerRadius = 15
        self.buttonView.layer.borderWidth = 2
        self.buttonView.layer.borderColor = UIColor.white.cgColor
        self.buttonView.layer.cornerRadius = 15
        self.buttonView.backgroundColor = UIColor.black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
