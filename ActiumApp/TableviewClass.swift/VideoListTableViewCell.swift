//
//  VideoListTableViewCell.swift
//  ActiumApp
//
//  Created by Rahul Mishra on 28/11/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit

class VideoListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblVideoPath: UILabel!
    @IBOutlet weak var listIconImage: UIImageView!
    
    @IBOutlet weak var deleteBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
