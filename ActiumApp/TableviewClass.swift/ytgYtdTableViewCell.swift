//
//  ytgYtdTableViewCell.swift
//  ActiumApp
//
//  Created by Rahul Mishra on 17/12/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit

class ytgYtdTableViewCell: UITableViewCell {
    @IBOutlet weak var ytgLbl: UILabel!
    @IBOutlet weak var ytdLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
