//
//  ChoosePhotoViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 26/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit

class ChoosePhotoViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet var roundView: [UIView]!
     @IBOutlet weak var submitBtn: UIButton!
    
    // MARK: - Viewa Life Cycle
    // Just for learing the commit
    override func viewDidLoad() {
        super.viewDidLoad()

        for item in roundView {
            item.layer.borderWidth = 1
            item.layer.borderColor = UIColor.lightGray.cgColor
            item.layer.cornerRadius = 15
            
        }
        
            submitBtn.layer.borderWidth = 1
            submitBtn.layer.borderColor = UIColor.lightGray.cgColor
            submitBtn.layer.cornerRadius = 15
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    @IBAction func backBarBtn(_ sender: UIBarButtonItem) {
        
            self.navigationController?.popViewController(animated: true)
        
    }
}
