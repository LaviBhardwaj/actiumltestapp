//
//  ChangePasswordViewController.swift
//  ActiumApp
//
//  Created by Rahul Mishra on 24/11/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var oldPasstxt: UITextField!
    @IBOutlet weak var newPassTxt: UITextField!
    
    
    // MARK: - View's LIfe Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        oldPasstxt.isSecureTextEntry = true
        newPassTxt.isSecureTextEntry = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAtions
    @IBAction func changePassBtnActn(_ sender: UIButton) {
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.changePassword()
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            CommonClass.showAlert("There is no Internet Connection")
        }
    }
    
    @IBAction func cancelBtnActn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func hideOldPassBtnActn(_ sender: UIButton) {
        if sender.isSelected == false{
            sender.isSelected = true
            oldPasstxt.isSecureTextEntry = false
        }
        else {
            sender.isSelected = false
            oldPasstxt.isSecureTextEntry = true
        }
    }
    @IBAction func hideNewPassBtnActn(_ sender: UIButton) {
        if sender.isSelected == false{
            sender.isSelected = true
            newPassTxt.isSecureTextEntry = false
        }
        else {
            sender.isSelected = false
            newPassTxt.isSecureTextEntry = true
        }
    }
    
    // MARK: - Apis
    func changePassword() {
        let params = ["oldPassword":oldPasstxt.text,"newPassword":newPassTxt.text,"userid":UserDefaults.standard.string(forKey: "userid")]
        
        CommonClass.getDataFromServer("updatePassword", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") { result -> Void in
            print(result)
            CommonClass.hideLoader()
            if let dictResponse = result as? [String: Any] {
                if let status = dictResponse["status"] as? Bool,
                    status == true {
                    guard let message = dictResponse["message"] as? String else {
                        return
                    }
                    let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(action)
                    
                    self.present(alertController, animated: true, completion: nil)
             }
                guard let message = dictResponse["message"] as? String else {
                    return
                }
                let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
                
                let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    
                }
                alertController.addAction(action)
                
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
}
