//
//  ProfileViewController.swift
//  ActiumApp
//
//  Created by Techsaga Corporations on 31/10/18.
//  Copyright © 2018 Techsaga Corporations. All rights reserved.
//

import UIKit
import Alamofire
import MMDrawController
import CoreData

class ProfileViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var imgprofile: UIImageView!
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var lastNameText: UITextField!
    @IBOutlet weak var mobileText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet var roundView: [UIView]!
    @IBOutlet weak var editInfoLbl: UILabel!
    @IBOutlet weak var editInfoImg: UIImageView!
    @IBOutlet weak var CameraIconView: UIView!
    
    // MARK: - Properties
    var profileDict = [String: Any]()
    var editInfo = Bool()
    var updatedProfileImage = UIImage()
    var profilePic = UIImage()
    var profile = [Profile]()
    
    // MARK: - View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.userProfileDetail()
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            self.profile = DataBaseHelper.shareInstance.getProfileData()
            self.setUpProfileOffline()
        }
        self.navigationController?.isNavigationBarHidden = true
        
        for item in roundView {
            item.layer.cornerRadius = 5
        }
        
        CameraIconView.layer.cornerRadius = CameraIconView.frame.size.height/2
        
        firstNameTxt.isUserInteractionEnabled = false
        lastNameText.isUserInteractionEnabled = false
        mobileText.isUserInteractionEnabled = false
        emailText.isUserInteractionEnabled = false
        
        imgprofile.layer.borderWidth = 1
        imgprofile.layer.borderColor = UIColor.lightGray.cgColor
        imgprofile.layer.masksToBounds = false
        imgprofile.layer.cornerRadius = imgprofile.frame.height/2
        imgprofile.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setUpProfileOffline() {
        if let firstName = self.profile[0].firstName {
            self.firstNameTxt.text = firstName
        }
        if let lastName = self.profile[0].lastName {
            self.lastNameText.text = lastName
        }
        if let mobileNo = self.profile[0].mobile {
            self.mobileText.text = mobileNo
        }
        if let emailId = self.profile[0].email {
            self.emailText.text = emailId
        }
        if let imageUrl = self.profile[0].image {
            if let url = NSURL(string: imageUrl) {
                self.imgprofile.af_setImage(withURL: url as URL)
            }
        }
        else {
            self.imgprofile.image = #imageLiteral(resourceName: "dummy_profile")
        }
    }
    
    // MARK: - IBActions
    @IBAction func backBarBtn(_ sender: UIBarButtonItem) {
        if let drawer = self.drawer() ,
            let manager = drawer.getManager(direction: .left){
            let value = !manager.isShow
            drawer.showLeftSlider(isShow: value)
        }
    }
    
    @IBAction func editInfoBtn(_ sender: UIButton) {
        if editInfo == false {
            editInfoLbl.text = "Save"
            editInfoImg.image = #imageLiteral(resourceName: "save")
            firstNameTxt.isUserInteractionEnabled = true
            lastNameText.isUserInteractionEnabled = true
            mobileText.isUserInteractionEnabled = true
            emailText.isUserInteractionEnabled = true
            editInfo = true
        }
        else {
            CommonClass.NetworkManager.isReachable { networkManagerInstance in
                self.updateProfile()
                self.editInfoLbl.text = "Edit Info"
                self.editInfoImg.image = #imageLiteral(resourceName: "edit")
                self.firstNameTxt.isUserInteractionEnabled = false
                self.lastNameText.isUserInteractionEnabled = false
                self.mobileText.isUserInteractionEnabled = false
                self.emailText.isUserInteractionEnabled = false
                self.editInfo = false
            }
            CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
                CommonClass.showAlert("There is no Internet Connection")
            }
        }
    }
    
    @IBAction func changePassBtn(_ sender: UIButton) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "ChangePasswordViewController") as? ChangePasswordViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func updateProfilePicBtnActn(_ sender: UIButton) {
        CommonClass.NetworkManager.isReachable { networkManagerInstance in
            self.updateProfilePictureAction()
        }
        CommonClass.NetworkManager.isUnreachable { networkManagerInstance in
            CommonClass.showAlert("There is no Internet Connection")
        }
    }
    
    
    // MARK: - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            updatedProfileImage = image
            uploadImageToServer()
        }else{
            print("Something went wrong")
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Functions
    func updateProfilePictureAction() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let actionGallery = UIAlertAction(title: "Photo Gallery", style: .default) { (action:UIAlertAction) in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        let actionCamera = UIAlertAction(title: "Camera", style: .default) { (action:UIAlertAction) in
            
            
            let  imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        let actionCancel = UIAlertAction(title: "cancel", style: .cancel) { (action:UIAlertAction) in
        }
        alertController.addAction(actionGallery)
        alertController.addAction(actionCamera)
        alertController.addAction(actionCancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Apis
    func updateProfile() {
        
        let url = URL(string: "http://3.0.78.175/actium-dev/api/user/updateProfileData")
        
        let params : [String : AnyObject] = [
            "userid":UserDefaults.standard.string(forKey: "userid") as AnyObject,
            "first_name": firstNameTxt.text as AnyObject ,
            "last_name": lastNameText.text as AnyObject,
            "mobile": mobileText.text as AnyObject,
            "email": emailText.text as AnyObject
        ]
        CommonClass.showLoader()
        Alamofire.request(url!, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .validate()
            .responseJSON(completionHandler: { (data) in
                CommonClass.hideLoader()
                switch data.result {
                case .success(let dataa):
                    if let dictResponse = dataa as? [String: Any] {
                        if let status = dictResponse["status"] as? Bool,
                            status == true {
                            
                            let alertController = UIAlertController(title: "Alert!", message: dictResponse["message"] as? String, preferredStyle: .alert)
                            
                            let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                                
                            }
                            alertController.addAction(action)
                            self.present(alertController, animated: true, completion: nil)
                        }
                        let alertController = UIAlertController(title: "Alert!", message: dictResponse["message"] as? String, preferredStyle: .alert)
                        
                        let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                            
                        }
                        alertController.addAction(action)
                        self.present(alertController, animated: true, completion: nil)
                    }
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func userProfileDetail() {
        
        CommonClass.showLoader()
        let params = ["userid":UserDefaults.standard.string(forKey: "userid")]
        
        CommonClass.getDataFromServer("userProfileDetail", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") { result -> Void in
            CommonClass.hideLoader()
            if let dictResponse = result as? [String: Any] {
                if let status = dictResponse["status"] as? Bool,
                    status == true {
                    guard let userData = dictResponse["userdata"] as? [String: Any] else {
                        return
                    }
                    self.profileDict = userData
                    if let Imageurl = userData["profile_image"] as? String {
                        if let url = NSURL(string: Imageurl) {
                            self.imgprofile.af_setImage(withURL: url as URL)
                        }
                        else {
                            self.imgprofile.image = #imageLiteral(resourceName: "dummy_profile")
                        }
                    }
                    if let firstName = self.profileDict["first_name"] as? String {
                        self.firstNameTxt.text = firstName
                    }
                    if let lastName = self.profileDict["last_name"] as? String {
                        self.lastNameText.text = lastName
                    }
                    if let mobileNo = self.profileDict["mobile"] as? String {
                        self.mobileText.text = mobileNo
                    }
                    if let emailId = self.profileDict["email"] as? String {
                        self.emailText.text = emailId
                    }
                }
                else {
                    let message = dictResponse["message"] as? String
                    let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    func uploadImageToServer() {
     
        let compressingValue:CGFloat = 0.2
        let data:Data = UIImageJPEGRepresentation(updatedProfileImage, compressingValue)!
        let imageStrng = data.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
        
        let params = ["userid":UserDefaults.standard.string(forKey: "userid") as Any,
                      "image_string":imageStrng] as [String : Any]
        CommonClass.showLoader()
    CommonClass.getDataFromServer("uploadProfileImage", parameter: params as [String : AnyObject], authentication: false, methodType: ".POST") { result -> Void in
        CommonClass.hideLoader()
        
                if let dictResponse = result as? [String: Any] {
                   if let status = dictResponse["status"] as? Bool,
                      status == true {
                    
                    self.imgprofile.image = self.updatedProfileImage
                    guard (dictResponse["profile-image"] as? String) != nil else {
                        return
                    }
                    
                    guard let message = dictResponse["message"] as? String else {
                        return
                    }
                    let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                        print("You've pressed cancel")
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                   }
                    else {
                        let message = dictResponse["message"] as? String
                        let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
                        
                        let action = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction) in
                            print("You've pressed cancel")
                        }
                        alertController.addAction(action)
                        self.present(alertController, animated: true, completion: nil)
                    }
            }
       }
    }
}
